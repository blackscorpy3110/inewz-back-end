package post.repository;

import com.higgsup.interestnews.backend.BackendApp;
import com.higgsup.interestnews.backend.presentation.enums.PostStatus;
import com.higgsup.interestnews.backend.repository.CategoryRepository;
import com.higgsup.interestnews.backend.repository.PostRepository;
import com.higgsup.interestnews.backend.repository.PostTemporaryRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.Category;
import com.higgsup.interestnews.backend.service.model.Post;
import com.higgsup.interestnews.backend.service.model.PostTemporary;
import com.higgsup.interestnews.backend.service.model.User;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BackendApp.class)
@WebIntegrationTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class postRepositoryTest {

    @Autowired
    PostRepository postRepository;

    @Autowired
    PostTemporaryRepository postTemporaryRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;


    @Test
    public void testAsetupOneForEachTest() {
        System.out.println("Start");
        postRepository.deleteAll();


        User user = userRepository.findOne(1);

        Category category = categoryRepository.findOne(2);

        PostTemporary postTemporary = new PostTemporary();
        postTemporary.setUserId(user);
        postTemporary.setCategoryId(category);
        postTemporary.setPostId(0);
        postTemporary.setPostTmpTitle("Tieu de thu 1");
        postTemporary.setPostTmpContent("Noi dung thu 1");
        postTemporary.setPostTmpDate(new Date());
        postTemporary.setPostTmpImage("image1.jpg");
        postTemporary.setPostTmpVersion(1);
        postTemporary.setPostTmpStatus("PENDING");
        postTemporaryRepository.save(postTemporary);

        PostTemporary postTemporary2 = new PostTemporary();
        postTemporary2.setUserId(user);
        postTemporary2.setCategoryId(category);
        postTemporary2.setPostId(0);
        postTemporary2.setPostTmpTitle("Tieu de thu 2");
        postTemporary2.setPostTmpContent("Noi dung thu 2");
        postTemporary2.setPostTmpDate(new Date());
        postTemporary2.setPostTmpImage("image2.jpg");
        postTemporary2.setPostTmpVersion(1);
        postTemporary2.setPostTmpStatus("PENDING");
        postTemporaryRepository.save(postTemporary2);

        PostTemporary postTemporary3 = new PostTemporary();
        postTemporary3.setUserId(user);
        postTemporary3.setCategoryId(category);
        postTemporary3.setPostId(0);
        postTemporary3.setPostTmpTitle("Tieu de thu 3");
        postTemporary3.setPostTmpContent("Noi dung thu 3");
        postTemporary3.setPostTmpDate(new Date());
        postTemporary3.setPostTmpImage("image3.jpg");
        postTemporary3.setPostTmpVersion(1);
        postTemporary3.setPostTmpStatus("PENDING");
        postTemporaryRepository.save(postTemporary3);

        PostTemporary postTemporary4 = new PostTemporary();
        postTemporary4.setUserId(user);
        postTemporary4.setCategoryId(category);
        postTemporary4.setPostId(0);
        postTemporary4.setPostTmpTitle("Tieu de thu 4");
        postTemporary4.setPostTmpContent("Noi dung thu 4");
        postTemporary4.setPostTmpDate(new Date());
        postTemporary4.setPostTmpImage("image4.jpg");
        postTemporary4.setPostTmpVersion(1);
        postTemporary4.setPostTmpStatus("PENDING");
        postTemporaryRepository.save(postTemporary4);

    }

    @Test
    public void testBShowAll() {
        System.out.println("test show all temporary post");
        List<PostTemporary> postTemporaries = (List<PostTemporary>) postTemporaryRepository.findAll();

        Assert.assertEquals(4, postTemporaries.size());

        PostTemporary postTemporary = postTemporaries.get(3);

        Assert.assertEquals(1, postTemporary.getUserId().getId());
        Assert.assertEquals(2, postTemporary.getCategoryId().getCategoryID().byteValue());
        Assert.assertEquals(0, postTemporary.getPostId());
        Assert.assertEquals("Tieu de thu 4", postTemporary.getPostTmpTitle());
        Assert.assertEquals("Noi dung thu 4", postTemporary.getPostTmpContent());
        Assert.assertEquals("image4.jpg", postTemporary.getPostTmpImage());
        Assert.assertEquals((Integer) 1, postTemporary.getPostTmpVersion());
        Assert.assertEquals("PENDING", postTemporary.getPostTmpStatus());

    }

    @Test
    public void testCShowOne() {
        System.out.println("show one post temporary");
        PostTemporary postTemporary = postTemporaryRepository.findOne(4);

        Assert.assertEquals(1, postTemporary.getUserId().getId());
        Assert.assertEquals(2, postTemporary.getCategoryId().getCategoryID().byteValue());
        Assert.assertEquals(0, postTemporary.getPostId());
        Assert.assertEquals("Tieu de thu 4", postTemporary.getPostTmpTitle());
        Assert.assertEquals("Noi dung thu 4", postTemporary.getPostTmpContent());
        Assert.assertEquals("image4.jpg", postTemporary.getPostTmpImage());
        Assert.assertEquals((Integer) 1, postTemporary.getPostTmpVersion());
        Assert.assertEquals("PENDING", postTemporary.getPostTmpStatus());
    }

    @Test
    public void testDCreateNewPost() {
        User user = userRepository.findOne(2);

        Category category = categoryRepository.findOne(3);

        PostTemporary postTemporary = new PostTemporary();
        postTemporary.setUserId(user);
        postTemporary.setCategoryId(category);
        postTemporary.setPostId(0);
        postTemporary.setPostTmpTitle("Tieu de thu 5");
        postTemporary.setPostTmpContent("Noi dung thu 5");
        postTemporary.setPostTmpDate(new Date());
        postTemporary.setPostTmpImage("image5.jpg");
        postTemporary.setPostTmpStatus("PENDING");
        postTemporary.setPostTmpVersion(1);

        postTemporaryRepository.save(postTemporary);

        PostTemporary newPostTemp = postTemporaryRepository.findOne(5);

        Assert.assertEquals(2, newPostTemp.getUserId().getId());
        Assert.assertEquals(3, newPostTemp.getCategoryId().getCategoryID().byteValue());
        Assert.assertEquals(0, postTemporary.getPostId());
        Assert.assertEquals("Tieu de thu 5", postTemporary.getPostTmpTitle());
        Assert.assertEquals("Noi dung thu 5", postTemporary.getPostTmpContent());
        Assert.assertEquals("image5.jpg", postTemporary.getPostTmpImage());
        Assert.assertEquals((Integer) 1, postTemporary.getPostTmpVersion());
    }

    @Test
    public void testEEditTemporaryPost() {
        System.out.println("Test edit temporary post");

        int testId = 5;

        PostTemporary postTemporary = postTemporaryRepository.findOne(testId);

        Assert.assertEquals(3, postTemporary.getCategoryId().getCategoryID().byteValue());

        Category category = categoryRepository.findOne(2);

        postTemporary.setCategoryId(category);

        postTemporaryRepository.save(postTemporary);

        postTemporaryRepository.findOne(testId);

        Assert.assertEquals(2, postTemporary.getCategoryId().getCategoryID().byteValue());

    }

    @Test
    public void testFDeleteTemporaryPost() {
        PostTemporary postTemporary = postTemporaryRepository.findOne(5);

        postTemporaryRepository.delete(postTemporary);

        postTemporary = postTemporaryRepository.findOne(5);

        Assert.assertNull(postTemporary);
    }

    @Test
    public void testGPublishedTemporaryPostWithTheFirstPublish() {
        PostTemporary postTemporary = postTemporaryRepository.findOne(1);

        if (postTemporary.getPostId() == 0) {
            Post post = new Post();
            post.setPostTitle(postTemporary.getPostTmpTitle());
            post.setPostTmpID(postTemporary.getPostTmpID());
            post.setPostContent(postTemporary.getPostTmpContent());
            post.setUserId(postTemporary.getUserId());
            post.setCategoryId(postTemporary.getCategoryId());
            post.setPostDate(postTemporary.getPostTmpDate());
            post.setPostVersion(postTemporary.getPostTmpVersion());
            post.setPostImage(postTemporary.getPostTmpImage());
            post.setPostStatus(PostStatus.PUBLISHED.getString());

            postRepository.save(post);

            Post newPost = postRepository.findOne(1);
            Assert.assertEquals("PUBLISHED", newPost.getPostStatus());
        }
    }

    @Test
    public void testHUpdateTmpPostWhenPublished() {
        Post newPost = postRepository.findOne(1);

        Integer postId = newPost.getPostId();
        Integer tmpPostId = newPost.getPostTmpID();
        String postStatus = PostStatus.ONLINE.getString();

        Boolean newTmpPost = postTemporaryRepository.updateTmpPostWhenPublished(tmpPostId, postId, postStatus);
        Assert.assertTrue(newTmpPost);
    }

    @Test
    public void testIEditPublishedPost() {
        //show post need to edit
        Post post = postRepository.findOne(1);

        //after edit that post,insert it to temporary with new a version and status
        PostTemporary postTemporary = new PostTemporary();

        postTemporary.setUserId(post.getUserId());
        postTemporary.setCategoryId(post.getCategoryId());
        postTemporary.setPostId(post.getPostId());
        postTemporary.setPostTmpTitle(post.getPostTitle());
        postTemporary.setPostTmpContent(post.getPostContent());
        postTemporary.setPostTmpDate(post.getPostDate());
        postTemporary.setPostTmpImage(post.getPostImage());
        postTemporary.setPostTmpStatus(PostStatus.PENDING.getString());
        postTemporary.setPostTmpVersion(post.getPostVersion() + 1);
        postTemporaryRepository.save(postTemporary);

        PostTemporary newPost = postTemporaryRepository.findOne(6);

        Assert.assertEquals("PENDING", newPost.getPostTmpStatus());
        Assert.assertEquals((Integer) 2, newPost.getPostTmpVersion());
    }

    @Test
    public void testKPublishedTemporaryPostToTheExistingPost() {
        PostTemporary postTemporary = postTemporaryRepository.findOne(6);
        if (postTemporary.getPostId() > 0) {

            Integer tmpId = postTemporary.getPostTmpID();
            String tit = postTemporary.getPostTmpTitle();
            String con = postTemporary.getPostTmpContent();
            Date d = postTemporary.getPostTmpDate();
            String img = postTemporary.getPostTmpImage();
            String stt = PostStatus.PUBLISHED.getString();
            Integer ver = postTemporary.getPostTmpVersion();
            Integer pId = postTemporary.getPostId();

            Boolean newPost = postTemporaryRepository.publishExistingPost(tmpId, tit, con, d, img, stt, ver, pId);

            Assert.assertTrue(newPost);
        }
    }

    @Test
    public void testLUpdateStatusWhenPublishPostAtTemporary() {
        PostTemporary postTemporary = postTemporaryRepository.findOne(6);
        //5: set online for post was published in post temporary
        if (postTemporaryRepository.updateTmpPostWhenPublished(postTemporary.getPostTmpID(), postTemporary.getPostId(), PostStatus.ONLINE.getString()) == true) {

            Integer tmpId = postTemporary.getPostTmpID();
            Integer pId = postTemporary.getPostId();
            //6: and set pending for all old post with the same post_id in post temporary
            postTemporaryRepository.updateStatusPostWhenPublishedNewVersion(tmpId, pId, PostStatus.PENDING.getString());

        }
        Assert.assertEquals("ONLINE", postTemporary.getPostTmpStatus());
    }


    @Test
    public void testMFindPostByUserId() {
        Assert.assertEquals(1, postRepository.findPostByUserId(1).size());
    }

    @Test
    public void testNFindPostByCatId() {
        Assert.assertEquals(1, postRepository.findPostByCategoryId(2).size());
    }

    @Test
    public void testOEverythingOkInPost() {
        Assert.assertEquals(1, ((List) postRepository.findAll()).size());
    }

    @Test
    public void testZEverythingOkInPostTemporary() {
        Assert.assertEquals(5, ((List) postTemporaryRepository.findAll()).size());
    }
}
