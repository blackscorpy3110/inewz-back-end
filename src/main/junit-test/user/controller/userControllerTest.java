package user.controller;

import com.higgsup.interestnews.backend.BackendApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

/**
 * Created by Mr.Java on 1/20/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {BackendApp.class})
@WebAppConfiguration
@IntegrationTest({"server.port:8089"})
public class userControllerTest {
    HttpHeaders requestHeaders;
    HttpEntity<String> requestEntity;
    private RestTemplate restTemplate = new RestTemplate();

    public userControllerTest() {
    }

    private void setRequestEntity() {
        ArrayList acceptableMediaTypes = new ArrayList();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        this.requestHeaders = new HttpHeaders();
        this.requestHeaders.setAccept(acceptableMediaTypes);
        this.requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.requestEntity = new HttpEntity(this.requestHeaders);
    }

    @Test
    public void testGetUsers() {
        this.setRequestEntity();
        ResponseEntity entity = this.restTemplate.exchange("http://localhost:8089/showAllUser", HttpMethod.GET, this.requestEntity, String.class, new Object[0]);
        String body = (String) entity.getBody();
        MediaType contentType = entity.getHeaders().getContentType();
        //int uid = Integer.parseInt(body.toString());
    }
}
