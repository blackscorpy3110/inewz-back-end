package user.repository;

import com.higgsup.interestnews.backend.BackendApp;
import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import com.higgsup.interestnews.backend.repository.RoleRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.Role;
import com.higgsup.interestnews.backend.service.model.User;
import com.higgsup.interestnews.backend.utils.MD5utils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import user.AnnotationUtils;

import java.util.List;

/**
 * Created by Mr.Java on 1/20/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BackendApp.class)
@WebIntegrationTest
@Transactional
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class userRepositoryTest {
    static int totalTests = 0;
    private static boolean setupDone = false;
    int count = 0;
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @BeforeClass
    public static void setupOneForWholeClass() {
        System.out.println("Start up - Class");

        // Count number of tests we have
        totalTests = AnnotationUtils.countMethodsWithAnnotation(userRepositoryTest.class, Test.class);
    }

    @AfterClass
    public static void teardownOneForWholeClass() {
        System.out.println("Teardown - Class");
    }

    @Before
    public void Test1Create() {
        //step 1: Clear data
//        userRepository.deleteAll();
        //step 2: Insert data test
        Role role = roleRepository.findOne(1);

        User user = new User();
        user.setFullName("Nguyen Khanh Toan");
        user.setUserName("toannk");
        user.setPassword(MD5utils.md5("khanhtoan"));
        user.setUserEmail("toannk@gmail.com");
        user.setUserDescription("My name toan. My full name is Nguyen Khanh Toan");
        user.setUserAvatar("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg");
        user.setRole(role);
        userRepository.save(user);

        User user1 = new User();
        user1.setFullName("Ha Khanh Toan");
        user1.setUserName("toannh");
        user1.setPassword(MD5utils.md5("khanhtoan"));
        user1.setUserEmail("toannh@gmail.com");
        user1.setUserDescription("My name toan. My full name is Ha Khanh Toan");
        user1.setUserAvatar("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg");
        user1.setRole(role);
        userRepository.save(user1);

        User user2 = new User();
        user2.setFullName("Tran Khanh Toan");
        user2.setUserName("toannt");
        user2.setPassword(MD5utils.md5("khanhtoan"));
        user2.setUserEmail("toannt@gmail.com");
        user2.setUserDescription("My name toan. My full name is Tran Khanh Toan");
        user2.setUserAvatar("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg");
        user2.setRole(role);
        userRepository.save(user2);

        User user3 = new User();
        user3.setFullName("Ngo Khanh Toan");
        user3.setUserName("toannn");
        user3.setPassword(MD5utils.md5("khanhtoan"));
        user3.setUserEmail("toannn@gmail.com");
        user3.setUserDescription("My name toan. My full name is Ngo Khanh Toan");
        user3.setUserAvatar("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg");
        user3.setRole(role);
        userRepository.save(user3);
        setupDone = true;
    }

    //Step 3 TEST


    @Test
    public void Test2SelectAll() {
        List<User> users = (List<User>) userRepository.findAll();
        Assert.assertEquals(4, users.size());

        User user = users.get(0);
        Assert.assertEquals(1, user.getId());
        Assert.assertEquals("Nguyen Khanh Toan", user.getFullName());
        Assert.assertEquals("toannk", user.getUserName());
        Assert.assertEquals(MD5utils.md5("khanhtoan"), user.getPassword());
        Assert.assertEquals("toannk@gmail.com", user.getUserEmail());
        Assert.assertEquals("My name toan. My full name is Nguyen Khanh Toan", user.getUserDescription());
        Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", user.getUserAvatar());
        Assert.assertEquals(1, user.getRole().getId());

        User user1 = users.get(1);
        Assert.assertEquals(2, user1.getId());
        Assert.assertEquals("Ha Khanh Toan", user1.getFullName());
        Assert.assertEquals("toannh", user1.getUserName());
        Assert.assertEquals(MD5utils.md5("khanhtoan"), user1.getPassword());
        Assert.assertEquals("toannh@gmail.com", user1.getUserEmail());
        Assert.assertEquals("My name toan. My full name is Ha Khanh Toan", user1.getUserDescription());
        Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", user1.getUserAvatar());
        Assert.assertEquals(1, user1.getRole().getId());

        User user2 = users.get(2);
        Assert.assertEquals(3, user2.getId());
        Assert.assertEquals("Tran Khanh Toan", user2.getFullName());
        Assert.assertEquals("toannt", user2.getUserName());
        Assert.assertEquals(MD5utils.md5("khanhtoan"), user2.getPassword());
        Assert.assertEquals("toannt@gmail.com", user2.getUserEmail());
        Assert.assertEquals("My name toan. My full name is Tran Khanh Toan", user2.getUserDescription());
        Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", user2.getUserAvatar());
        Assert.assertEquals(1, user2.getRole().getId());

        User user3 = users.get(3);
        Assert.assertEquals(4, user3.getId());
        Assert.assertEquals("Ngo Khanh Toan", user3.getFullName());
        Assert.assertEquals("toannn", user3.getUserName());
        Assert.assertEquals(MD5utils.md5("khanhtoan"), user3.getPassword());
        Assert.assertEquals("toannn@gmail.com", user3.getUserEmail());
        Assert.assertEquals("My name toan. My full name is Ngo Khanh Toan", user3.getUserDescription());
        Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", user3.getUserAvatar());
        Assert.assertEquals(1, user3.getRole().getId());
    }

    @Test
    public void Test3SelectOne() {
        User user = userRepository.findOne(6);

//        Assert.assertEquals(2, user.getId());
        Assert.assertEquals("Ha Khanh Toan", user.getFullName());
//        Assert.assertEquals("toannh", user.getUserName());
//        Assert.assertEquals(MD5utils.md5("khanhtoan"), user.getPassword());
//        Assert.assertEquals("toannh@gmail.com", user.getUserEmail());
//        Assert.assertEquals("My name toan. My full name is Ha Khanh Toan", user.getUserDescription());
//        Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", user.getUserAvatar());
//        Assert.assertEquals(1, user.getRole().getId());
    }

    @Test
    public void Test4byUserName() {
//        List<UserDTO> users = userRepository.findByUserName("Ngo Khanh Toan");
//        for (UserDTO dto : users) {
//            Assert.assertEquals(4, dto.getUserId());
//            Assert.assertEquals("Ngo Khanh Toan", dto.getFullName());
//            Assert.assertEquals("toannn", dto.getUsername());
//            Assert.assertEquals(MD5utils.md5("khanhtoan"), dto.getPassword());
//            Assert.assertEquals("toannn@gmail.com", dto.getUserEmail());
//            Assert.assertEquals("My name toan. My full name is Ngo Khanh Toan", dto.getUserDescription());
//            Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", dto.getUserAvatar());
//            Assert.assertEquals(1, dto.getRoleID());
//
//        }
    }

    @Test
    public void Test5byUserEmail() {
        List<UserDTO> users = userRepository.findByUserEmail("toanntt@gmail.com");
        for (UserDTO dto : users) {
            Assert.assertEquals(3, dto.getUserId());
            Assert.assertEquals("Tran Khanh Toan", dto.getFullName());
            Assert.assertEquals("toannt", dto.getUsername());
            Assert.assertEquals(MD5utils.md5("khanhtoan"), dto.getPassword());
            Assert.assertEquals("toannt@gmail.com", dto.getUserEmail());
            Assert.assertEquals("My name toan. My full name is Tran Khanh Toan", dto.getUserDescription());
            Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg", dto.getUserAvatar());
            Assert.assertEquals(1, dto.getRoleID());

        }
    }

//    public void findbySession(){
//        List<UserDTO> users =  userRepository.findBySessionID("toannt@gmail.com");
//        for(UserDTO dto:users){
//            Assert.assertEquals(3,dto.getUserId());
//            Assert.assertEquals("Tran Khanh Toan", dto.getFullName());
//            Assert.assertEquals("toannt",dto.getUsername());
//            Assert.assertEquals(MD5utils.md5("khanhtoan"),dto.getPassword());
//            Assert.assertEquals("toannt@gmail.com",dto.getUserEmail());
//            Assert.assertEquals("My name toan. My full name is Tran Khanh Toan",dto.getUserDescription());
//            Assert.assertEquals("C:/Users/Mr.Java/Desktop/uploadfile/a2.jpg",dto.getUserAvatar());
//            Assert.assertEquals(1,dto.getRoleID());
//        }
//    }
@After
public void teardown() {
    System.out.println("Teardown");
    userRepository.deleteAll();
}
}
