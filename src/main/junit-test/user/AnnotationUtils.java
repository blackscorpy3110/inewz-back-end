package user;

import java.lang.reflect.Method;

public class AnnotationUtils {
    public static int countMethodsWithAnnotation(Class<?> klass,
                                                 Class annotation) {
        int count = 0;
        for (Method m : klass.getMethods()) {
            if (m.isAnnotationPresent(annotation)) {
                count++;
            }
        }
        return count;
    }
}