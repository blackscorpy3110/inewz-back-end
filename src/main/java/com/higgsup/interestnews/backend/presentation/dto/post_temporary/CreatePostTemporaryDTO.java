package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class CreatePostTemporaryDTO {
    private Integer tmpPostId;
    private Integer userId;
    private Integer categoryId;
    private Integer postId;
    private String tmpPostTitle;
    private String tmpPostContent;
    private Date tmpPostDate;

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
