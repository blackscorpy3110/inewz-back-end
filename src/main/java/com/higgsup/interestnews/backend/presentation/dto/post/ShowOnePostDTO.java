package com.higgsup.interestnews.backend.presentation.dto.post;

import com.higgsup.interestnews.backend.utils.GsonHelper;

/**
 * Created by Bui Cong Thanh on 2/16/2016.
 */
public class ShowOnePostDTO {
    private Integer userId;
    private Integer postId;
    private Integer catId;
    private String postTitle;
    private String postContent;
    private String catName;
    private String postImage;
    private Integer postVersion;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public Integer getPostVersion() {
        return postVersion;
    }

    public void setPostVersion(Integer postVersion) {
        this.postVersion = postVersion;
    }

    public ShowOnePostDTO(Integer userId, Integer postId, Integer catId,
                          String postTitle, String postContent, String catName,
                          String postImage, Integer postVersion) {
        this.userId = userId;
        this.postId = postId;
        this.catId = catId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.catName = catName;
        this.postImage = postImage;
        this.postVersion = postVersion;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
