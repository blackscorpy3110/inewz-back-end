package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/18/2016.
 */
public class ShowAllTemporaryPostListDTO {
    private List<ShowAllTemporaryPostDTO> allTemporaryPostDTOs = new ArrayList<ShowAllTemporaryPostDTO>();

    public List<ShowAllTemporaryPostDTO> getAllTemporaryPostDTOs() {
        return allTemporaryPostDTOs;
    }

    public void setAllTemporaryPostDTOs(List<ShowAllTemporaryPostDTO> allTemporaryPostDTOs) {
        this.allTemporaryPostDTOs = allTemporaryPostDTOs;
    }
}
