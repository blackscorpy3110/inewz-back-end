package com.higgsup.interestnews.backend.presentation.dto.common;

/**
 * Created by Mr.Java on 1/15/2016.
 */
public class SimpleDTO {
    private String message;

    public SimpleDTO() {
    }

    public SimpleDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
