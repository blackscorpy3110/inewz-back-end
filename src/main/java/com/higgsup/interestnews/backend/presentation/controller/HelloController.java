package com.higgsup.interestnews.backend.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
// @Controller
@RequestMapping("/hello")
public class HelloController {
    // http://localhost/hello

    @RequestMapping(path = "/firstHello", method = RequestMethod.GET)
    public String firstHello() {
        return "Say hello to me";
    }
}
