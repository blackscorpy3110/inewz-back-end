package com.higgsup.interestnews.backend.presentation.interceptor;

import com.higgsup.interestnews.backend.exception.AuthorizationException;
import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import com.higgsup.interestnews.backend.presentation.enums.Role;
import com.higgsup.interestnews.backend.service.Authentication_service.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Autowired
    IAuthenticationService authenticationService;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        System.out.println("Pre-handle Authentication Interceptor");

        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = (Method) handlerMethod.getMethod();
            String methodName = method.getName();

            SecureRole roleAnnotation = AnnotationUtils
                    .findAnnotation(method, SecureRole.class);

            if (roleAnnotation == null) return true; // Allow by default if no @SecureRole is annotated on the method

            String tokenInput = request.getParameter("token");
            UserDTO user = authenticationService.checkToken(tokenInput);
            Role currentRole = Role.valueOf(user.getRoleID().getRoleName());
            if (roleAnnotation.value().ordinal() > currentRole.ordinal()) {
                throw new AuthorizationException("You don't have required role to acccess the function");
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        System.out.println("Post-handle Authentication");
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        System.out.println("After completion handle Authentication");
    }
}