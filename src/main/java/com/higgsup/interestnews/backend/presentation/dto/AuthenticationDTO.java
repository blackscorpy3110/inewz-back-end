package com.higgsup.interestnews.backend.presentation.dto;

/**
 * Created by Bui Cong Thanh on 12/30/2015.
 */
public class AuthenticationDTO {
    private String token;
    private int role;
    private String fullName;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
