package com.higgsup.interestnews.backend.presentation.dto.category;

import com.higgsup.interestnews.backend.utils.GsonHelper;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public class CategoryDTO {
    private Integer catId;
    private String catName;
    private String catDescription;
    private String categoryImage;

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatDescription() {
        return catDescription;
    }

    public void setCatDescription(String catDescription) {
        this.catDescription = catDescription;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
