package com.higgsup.interestnews.backend.presentation.dto.User;

/**
 * Created by Mr.Java on 1/13/2016.
 */
public class DeleteUserRequestDTO {
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
