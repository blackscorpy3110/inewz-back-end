package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/18/2016.
 */
public class ShowAllTemporaryPostDTO {
    private Integer tmpPostId;
    private String fullName;
    private Integer catId;
    private String catName;
    private Integer postId;
    private String tmpPostTitle;
    private String tmpPostContent;
    private Date tmpPostDate;
    private String tmpPostImage;
    private String postTmpStatus;
    private Integer tmpPostVersion;
    private Integer userId;


    public ShowAllTemporaryPostDTO(Integer tmpPostId, String fullName, Integer catId, String catName,
                                   Integer postId, String tmpPostTitle, String tmpPostContent, Date tmpPostDate,
                                   String tmpPostImage, String postTmpStatus, Integer tmpPostVersion, Integer userId) {
        this.tmpPostId = tmpPostId;
        this.fullName = fullName;
        this.catId = catId;
        this.catName = catName;
        this.postId = postId;
        this.tmpPostTitle = tmpPostTitle;
        this.tmpPostContent = tmpPostContent;
        this.tmpPostDate = tmpPostDate;
        this.tmpPostImage = tmpPostImage;
        this.postTmpStatus = postTmpStatus;
        this.tmpPostVersion = tmpPostVersion;
        this.userId = userId;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    public String getTmpPostImage() {
        return tmpPostImage;
    }

    public void setTmpPostImage(String tmpPostImage) {
        this.tmpPostImage = tmpPostImage;
    }

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    public Integer getTmpPostVersion() {
        return tmpPostVersion;
    }

    public void setTmpPostVersion(Integer tmpPostVersion) {
        this.tmpPostVersion = tmpPostVersion;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
