package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/15/2016.
 */
public class SearchPostListDTO {
    List<SearchPostDTO> searchPostDTOs = new ArrayList<SearchPostDTO>();

    public List<SearchPostDTO> getSearchPostDTOs() {
        return searchPostDTOs;
    }

    public void setSearchPostDTOs(List<SearchPostDTO> searchPostDTOs) {
        this.searchPostDTOs = searchPostDTOs;
    }
}
