package com.higgsup.interestnews.backend.presentation.dto.common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Mr.Java on 1/15/2016.
 */
public class ErrorDTO {
    @JsonProperty
    private String statusCode;
    @JsonProperty
    private String statusDescription;
    @JsonProperty
    private String errorMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static interface Properties {
        public static final String STATUS_CODE = "statusCode";
        public static final String STATUS_DESCRIPTION = "statusDescription";
        public static final String ERROR_MESSAGE = "errorMessage";
    }
}
