package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.category.CategoryDTO;
import com.higgsup.interestnews.backend.presentation.dto.category.CategoryManageListDTO;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.enums.Role;
import com.higgsup.interestnews.backend.presentation.interceptor.SecureRole;
import com.higgsup.interestnews.backend.service.category_service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
@CrossOrigin
@RestController
public class CatController {
    @Autowired
    ICategoryService categoryService;


    // create new category (done)
    @RequestMapping(path = "/createNewCategory", method = RequestMethod.POST)
    @SecureRole(Role.EDITOR)
    public SimpleDTO createNewPost(@RequestParam("field") String field,
                                     @RequestParam("file") MultipartFile file) throws Exception {
        return categoryService.createCategory(field,file);
    }


    //delete category (done)
    @RequestMapping(path = "/deleteCategory", method = RequestMethod.POST)
    @SecureRole(Role.EDITOR)
    public SimpleDTO deleteCategory(@RequestBody CategoryDTO categoryDTO) throws Exception {
        return categoryService.deleteCategory(categoryDTO);
    }


    //show all category (done)
    @RequestMapping(path = "/showAllCategory", method = RequestMethod.GET)
    public CategoryManageListDTO showAllCategory() throws Exception {
        return categoryService.showAllCategory();
    }

    @RequestMapping(path = "/showOneCategory", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public CategoryManageListDTO showOneCategory(@RequestBody Integer catId) throws Exception {
        return categoryService.showOneCategory(catId);
    }

    //edit category (done)
    @RequestMapping(path = "/editCategory", method = RequestMethod.POST)
    @SecureRole(Role.EDITOR)
    public SimpleDTO editCategory(@RequestParam("field") String field,
                                    @RequestParam("file") MultipartFile file) throws Exception {
        return categoryService.editCategory(field,file);
    }
}
