package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/12/2016.
 */
public class PostTemporaryManageListDTO {
    private List<PostTemporaryDTO> postTemporaryDTOs = new ArrayList<PostTemporaryDTO>();

    public List<PostTemporaryDTO> getPostTemporaries() {
        return postTemporaryDTOs;
    }

    public void setPostTemporaries(List<PostTemporaryDTO> postTemporaries) {
        this.postTemporaryDTOs = postTemporaries;
    }
}

