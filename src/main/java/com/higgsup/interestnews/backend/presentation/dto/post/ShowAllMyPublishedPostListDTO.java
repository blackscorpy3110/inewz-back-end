package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ShowAllMyPublishedPostListDTO {
    List<ShowAllMyPublishedPost> publishedPosts = new ArrayList<ShowAllMyPublishedPost>();

    public List<ShowAllMyPublishedPost> getPublishedPosts() {
        return publishedPosts;
    }

    public void setPublishedPosts(List<ShowAllMyPublishedPost> publishedPosts) {
        this.publishedPosts = publishedPosts;
    }
}
