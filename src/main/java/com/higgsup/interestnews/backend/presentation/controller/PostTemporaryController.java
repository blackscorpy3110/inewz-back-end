package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post_temporary.*;
import com.higgsup.interestnews.backend.presentation.enums.Role;
import com.higgsup.interestnews.backend.presentation.interceptor.SecureRole;
import com.higgsup.interestnews.backend.service.post_temporary_service.IPostTemporaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
@CrossOrigin
@RestController//co san produces va consumes application json va response body
public class PostTemporaryController {

    @Autowired
    IPostTemporaryService postTemporaryService;

    //create new post to temporary table (done)
    @RequestMapping(path = "/createNewPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public SimpleDTO createNewPost(@RequestParam("field") String field,
                                          @RequestParam("file") MultipartFile file,
                                          @RequestParam String token
    ) throws Exception {
        return postTemporaryService.createNewPost(token, field, file);
    }

    //edit temporary post by tmp post id (done)
    @RequestMapping(path = "/editTemporaryPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public SimpleDTO updatePostTemporaryDTO(@RequestParam("field") String field,
                                            @RequestParam("file") MultipartFile file) throws Exception {
        return postTemporaryService.updateTemporaryPost(field,file);
    }

    //show all temporary post for manage (done)
    @RequestMapping(path = "/showAllTemporaryPost", method = RequestMethod.GET)
    @SecureRole(Role.EDITOR)
    public ShowAllTemporaryPostListDTO showAllTemporaryPost() throws Exception {
        return postTemporaryService.showAllTemporaryPost();
    }

    //show all my temporary post for manage (done)
    @RequestMapping(path = "/showAllMyTemporaryPost", method = RequestMethod.GET)
    @SecureRole(Role.CONTRIBUTOR)
    public SearchTemporaryPostListDTO showAllMyTemporaryPost(HttpServletRequest request) throws Exception {
        String tokenInput = request.getParameter("token");
        return postTemporaryService.showAllMyTemporaryPost(tokenInput);
    }

    //view full one temporary post for manage (done)
    @RequestMapping(path = "/showSingleTemporaryPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public ShowAllTemporaryPostListDTO showOneTemporaryPost(@RequestBody Integer tmpPostId) throws Exception {
        return postTemporaryService.getOneTemporaryPostById(tmpPostId);
    }

    //delete one temporary post (done)
    @RequestMapping(path = "/deleteTemporaryPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public SimpleDTO deleteTemporaryPost(@RequestBody DeletePostTemporaryDTO deletePostTemporaryDTO) throws Exception {
        return postTemporaryService.deleteTemporaryPost(deletePostTemporaryDTO);
    }

    //publish one post form temporary to post table (done)
    @RequestMapping(path = "/publishPost", method = RequestMethod.POST)
    @SecureRole(Role.EDITOR)
    public SimpleDTO publishPost(@RequestBody PublishPostTemporaryDTO publishPostTemporaryDTO) throws Exception {
        return postTemporaryService.publishTemporaryPost(publishPostTemporaryDTO);
    }

}
