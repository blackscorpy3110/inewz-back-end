package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post.*;
import com.higgsup.interestnews.backend.presentation.enums.Role;
import com.higgsup.interestnews.backend.presentation.interceptor.SecureRole;
import com.higgsup.interestnews.backend.service.post_service.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@CrossOrigin
@RestController
public class PostController {
    @Autowired
    IPostService postService;

    //view post in home page
    @RequestMapping(path = "/homepage", method = RequestMethod.GET)
    public ViewAllPublishedPostListDTO viewHomePage() {
        return postService.viewAllPostInHomePage();
    }

    //view single post
    @RequestMapping(path = "/singlePost", method = RequestMethod.POST)
    public ViewSinglePostListDTO viewSinglePostById(@RequestBody Integer postId) {
        return postService.viewSinglePostById(postId);
    }

    @RequestMapping(path = "/morePostSameCategory", method = RequestMethod.POST)
    public ViewMorePostListDTO morePostSameCategory(@RequestBody ViewMorePostDTO viewMorePostDTO) {
        return postService.morePostSameCategory(viewMorePostDTO);
    }

    //show one post for edit
    @RequestMapping(path = "/showOnePost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public ShowOnePostListDTO showOnePost(@RequestBody Integer postId) {
        return postService.showOnePublishedPost(postId);
    }

    //show all published post for manage()
    @RequestMapping(path = "/showAllPublishedPost", method = RequestMethod.GET)
    @SecureRole(Role.CONTRIBUTOR)
    public ShowAllPublishedPostListDTO showAllPublishedPost() {
        return postService.showAllPublishedPost();
    }

    //show all published post for private person (done)
    @RequestMapping(path = "/showAllMyPublishedPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public ShowAllMyPublishedPostListDTO showAllMyPublishedPost(HttpServletRequest request) throws Exception {
        String tokenInput = request.getParameter("token");
        return postService.showAllMyPublishedPost(tokenInput);
    }

    //delete one publish post (done)
    @RequestMapping(path = "/deletePublishPost", method = RequestMethod.POST)
    @SecureRole(Role.EDITOR)
    public SimpleDTO deletePublishPost(@RequestBody DeletePostDTO deletePostDTO) throws Exception {
        return postService.deletePublishedPost(deletePostDTO);
    }

    //edit one published post
    @RequestMapping(path = "/editPublishedPost", method = RequestMethod.POST)
    @SecureRole(Role.CONTRIBUTOR)
    public SimpleDTO editPublishedPost(@RequestParam("field") String field,
                                              @RequestParam("file") MultipartFile file) throws Exception {
        return postService.editPublishedPost(field,file);
    }

    //find published post by user id
    @RequestMapping(path = "/findPostByUser", method = RequestMethod.POST)
    public SearchPostListDTO findPostByUser(@RequestBody Integer userId) throws Exception {
        return postService.searchPostByUserId(userId);
    }

    //find published post by category
    @RequestMapping(path = "/findPostByCategory", method = RequestMethod.POST)
    public SearchPostListDTO findPostByCategory(@RequestBody Integer catId) throws Exception {
        return postService.searchPostByCategoryId(catId);
    }


}
