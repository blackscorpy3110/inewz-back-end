package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class UpdatePostTemporaryDTO {
    private Integer catId;
    private Integer tmpPostId;
    private String tmpPostTitle;
    private String tmpPostContent;
    private Date tmpPostDate;
    private String tmpPostImage;
    private Integer tmpPostVersion;
    private String postTmpStatus;

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    public String getTmpPostImage() {
        return tmpPostImage;
    }

    public void setTmpPostImage(String tmpPostImage) {
        this.tmpPostImage = tmpPostImage;
    }

    public Integer getTmpPostVersion() {
        return tmpPostVersion;
    }

    public void setTmpPostVersion(Integer tmpPostVersion) {
        this.tmpPostVersion = tmpPostVersion;
    }

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }

}
