package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 2/16/2016.
 */
public class ShowOnePostListDTO {
    List<ShowOnePostDTO> showOnePostDTOs = new ArrayList<ShowOnePostDTO>();

    public List<ShowOnePostDTO> getShowOnePostDTOs() {
        return showOnePostDTOs;
    }

    public void setShowOnePostDTOs(List<ShowOnePostDTO> showOnePostDTOs) {
        this.showOnePostDTOs = showOnePostDTOs;
    }
}
