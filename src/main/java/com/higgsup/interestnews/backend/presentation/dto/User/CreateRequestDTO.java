package com.higgsup.interestnews.backend.presentation.dto.User;

/**
 * Created by Mr.Java on 1/13/2016.
 */
public class CreateRequestDTO {
    private String fullName;
    private String userName;
    private String userPassword;
    private String userEmail;
    private String userDescription;
    private String userAvatar;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public CreateRequestDTO(String fullName, String userName, String userPassword, String userEmail, String userDescription, String userAvatar) {
        this.fullName = fullName;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.userDescription = userDescription;
        this.userAvatar = userAvatar;
    }

    public CreateRequestDTO() {
    }
}
