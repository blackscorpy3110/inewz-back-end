package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class SearchTemporaryPostDTO {
    private Integer tmpPostId;
    private String catName;
    private String tmpPostTitle;
    private String tmpPostContent;
    private Date tmpPostDate;
    private String tmpPostImage;
    private String postTmpStatus;
    private Integer tmpPostVersion;
    private Integer userId;
    private Integer catId;
    private Integer postId;

    public SearchTemporaryPostDTO(Integer tmpPostId, String catName, String tmpPostTitle, String tmpPostContent,
                                  Date tmpPostDate, String tmpPostImage, String postTmpStatus, Integer tmpPostVersion,
                                  Integer userId, Integer catId, Integer postId) {
        this.tmpPostId = tmpPostId;
        this.catName = catName;
        this.tmpPostTitle = tmpPostTitle;
        this.tmpPostContent = tmpPostContent;
        this.tmpPostDate = tmpPostDate;
        this.tmpPostImage = tmpPostImage;
        this.postTmpStatus = postTmpStatus;
        this.tmpPostVersion = tmpPostVersion;
        this.userId = userId;
        this.catId = catId;
        this.postId = postId;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    public String getTmpPostImage() {
        return tmpPostImage;
    }

    public void setTmpPostImage(String tmpPostImage) {
        this.tmpPostImage = tmpPostImage;
    }

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    public Integer getTmpPostVersion() {
        return tmpPostVersion;
    }

    public void setTmpPostVersion(Integer tmpPostVersion) {
        this.tmpPostVersion = tmpPostVersion;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
