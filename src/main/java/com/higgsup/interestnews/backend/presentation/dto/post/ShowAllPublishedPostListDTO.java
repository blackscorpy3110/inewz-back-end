package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ShowAllPublishedPostListDTO {
    List<ShowAllPublishedPostDTO> publishedPostDTOList = new ArrayList<ShowAllPublishedPostDTO>();

    public List<ShowAllPublishedPostDTO> getPublishedPostDTOList() {
        return publishedPostDTOList;
    }

    public void setPublishedPostDTOList(List<ShowAllPublishedPostDTO> publishedPostDTOList) {
        this.publishedPostDTOList = publishedPostDTOList;
    }
}
