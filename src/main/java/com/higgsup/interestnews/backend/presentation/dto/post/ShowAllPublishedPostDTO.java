package com.higgsup.interestnews.backend.presentation.dto.post;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ShowAllPublishedPostDTO {
    private Integer tmpPostId;
    private Integer userId;
    private String fullName;
    private String catName;
    private Integer postId;
    private String postTitle;
    private String postContent;
    private Date postDate;
    private String postImage;
    private String postStatus;
    private Integer postVersion;

    public ShowAllPublishedPostDTO(Integer tmpPostId, Integer userId, String fullName, String catName, Integer postId,
                                   String postTitle, String postContent, Date postDate, String postImage,
                                   String postStatus, Integer postVersion) {
        this.tmpPostId = tmpPostId;
        this.userId = userId;
        this.fullName = fullName;
        this.catName = catName;
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postImage = postImage;
        this.postStatus = postStatus;
        this.postVersion = postVersion;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public Integer getPostVersion() {
        return postVersion;
    }

    public void setPostVersion(Integer postVersion) {
        this.postVersion = postVersion;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
