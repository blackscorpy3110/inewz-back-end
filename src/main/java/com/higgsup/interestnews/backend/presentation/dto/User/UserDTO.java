package com.higgsup.interestnews.backend.presentation.dto.User;

import com.higgsup.interestnews.backend.service.model.Role;
import com.higgsup.interestnews.backend.utils.GsonHelper;
/**
 * Created by lent on 12/24/2015.
 */
public class UserDTO{

    private String fullName;

    private String username;

    private Role roleID;

    private String password;

    private String sessionID;

    private String userEmail;

    private String userDescription;

    private String userAvatar;

    private int userId;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }



    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public Role getRoleID() {
        return roleID;
    }

    public void setRoleID(Role roleID) {
        this.roleID = roleID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
