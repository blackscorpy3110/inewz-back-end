package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/14/2016.
 */
public class PublishPostTemporaryDTO {
    private Integer tmpPostId;

    private Integer userId;

    private Integer catId;

    private Integer postId;

    private String tmpPostTitle;

    private String tmpPostContent;

    private Date tmpPostDate;

    private String tmpPostImage;

    private String postTmpStatus;

    private Integer tmpPostVersion;

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    public String getTmpPostImage() {
        return tmpPostImage;
    }

    public void setTmpPostImage(String tmpPostImage) {
        this.tmpPostImage = tmpPostImage;
    }

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    public Integer getTmpPostVersion() {
        return tmpPostVersion;
    }

    public void setTmpPostVersion(Integer tmpPostVersion) {
        this.tmpPostVersion = tmpPostVersion;
    }
}
