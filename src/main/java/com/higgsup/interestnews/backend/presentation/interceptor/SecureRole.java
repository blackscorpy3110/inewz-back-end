package com.higgsup.interestnews.backend.presentation.interceptor;

import com.higgsup.interestnews.backend.presentation.enums.Role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Bui Cong Thanh on 12/31/2015.
 */
@Retention(RetentionPolicy.RUNTIME)
// khai báo annotation này có hiệu lực với method
@Target(ElementType.METHOD)
public @interface SecureRole {
    Role value();
}
