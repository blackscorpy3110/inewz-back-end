package com.higgsup.interestnews.backend.presentation.enums;

/**
 * Created by Bui Cong Thanh on 1/15/2016.
 */
public enum PostStatus {
    PENDING(1),
    ONLINE(2),
    PUBLISHED(3),
    DELETED(4);

    private int value;

    PostStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getString() {
        switch (this) {
            case PENDING:
                return "PENDING";
            case ONLINE:
                return "ONLINE";
            case PUBLISHED:
                return "PUBLISHED";
            case DELETED:
                return "DELETED";
            default:
                return "PENDING";
        }
    }
}
