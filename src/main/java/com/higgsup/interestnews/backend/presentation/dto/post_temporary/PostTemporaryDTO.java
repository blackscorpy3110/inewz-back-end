package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.service.model.Category;
import com.higgsup.interestnews.backend.service.model.User;
import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class PostTemporaryDTO {

    private Integer tmpPostId;

    private User userId;

    private Category catId;

    private Integer postId;

    private String tmpPostTitle;

    private String tmpPostContent;

    private Date tmpPostDate;

    private String tmpPostImage;

    private String tmpPostStatus;

    private Integer tmpPostVersion;

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Category getCatId() {
        return catId;
    }

    public void setCatId(Category catId) {
        this.catId = catId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTmpPostTitle() {
        return tmpPostTitle;
    }

    public void setTmpPostTitle(String tmpPostTitle) {
        this.tmpPostTitle = tmpPostTitle;
    }

    public String getTmpPostContent() {
        return tmpPostContent;
    }

    public void setTmpPostContent(String tmpPostContent) {
        this.tmpPostContent = tmpPostContent;
    }

    public Date getTmpPostDate() {
        return tmpPostDate;
    }

    public void setTmpPostDate(Date tmpPostDate) {
        this.tmpPostDate = tmpPostDate;
    }

    public String getTmpPostImage() {
        return tmpPostImage;
    }

    public void setTmpPostImage(String tmpPostImage) {
        this.tmpPostImage = tmpPostImage;
    }

    public String getTmpPostStatus() {
        return tmpPostStatus;
    }

    public void setTmpPostStatus(String tmpPostStatus) {
        this.tmpPostStatus = tmpPostStatus;
    }

    public Integer getTmpPostVersion() {
        return tmpPostVersion;
    }

    public void setTmpPostVersion(Integer tmpPostVersion) {
        this.tmpPostVersion = tmpPostVersion;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
