package com.higgsup.interestnews.backend.presentation.dto.post;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ViewAllPublishedPostDTO {
    //p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,c.category_name,u.full_name
    private Integer postId;
    private String postTitle;
    private String postContent;
    private Date postDate;
    private String postImage;
    private String catName;
    private String fullName;
    private Integer userId;
    private Integer catId;

    public ViewAllPublishedPostDTO(Integer postId, String postTitle, String postContent,
                                   Date postDate, String postImage, String catName,
                                   String fullName, Integer userId, Integer catId) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postImage = postImage;
        this.catName = catName;
        this.fullName = fullName;
        this.userId = userId;
        this.catId = catId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
