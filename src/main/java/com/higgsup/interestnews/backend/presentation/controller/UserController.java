package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.User.*;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.enums.Role;
import com.higgsup.interestnews.backend.presentation.interceptor.SecureRole;
import com.higgsup.interestnews.backend.service.user.IUserService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    IUserService userService;

    @RequestMapping(path = "/register",method = RequestMethod.POST)
    public SimpleDTO createUser(@RequestBody CreateRequestDTO user) throws ServiceException {
        return userService.createUser(user);
    }

    @RequestMapping(path = "/showMyProfile",method = RequestMethod.POST)
    @ResponseBody
    public UserManageListDTO getOneUser(@RequestBody UserManageDTO userManageDTO)  {
        return userService.getOneUser(userManageDTO);
    }

    @RequestMapping(path = "/showAllUser", method = RequestMethod.GET)
    @SecureRole(Role.ADMIN)
    public UserManageListDTO showAllUser() throws Exception {
       return userService.getAllUser();
    }

    @RequestMapping(path = "/delUser", method = RequestMethod.POST)
    @SecureRole(Role.ADMIN)
    public SimpleDTO deleteUser(@RequestBody Integer userId) throws Exception {
        return userService.deleteUser(userId);
    }

    @RequestMapping(path = "/updateUser", method = RequestMethod.POST)
    @SecureRole(Role.ADMIN)
    public SimpleDTO updateUser(@RequestBody UpdateUserRequestDTO updateUserRequestDTO) throws Exception {
        return userService.updateUser(updateUserRequestDTO);
    }

    @RequestMapping(path = "/updateProfile",method = RequestMethod.POST)
    public String updateProfile(@RequestBody UpdateProfileRequestDTO updateProfileRequestDTO) throws Exception {
        return userService.updateProfile(updateProfileRequestDTO);
    }


}
