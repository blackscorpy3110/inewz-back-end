package com.higgsup.interestnews.backend.presentation.dto.User;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mr.Java on 1/12/2016.
 */
public class UserDTOs {
    @Expose
    private List<UserDTO> userList = new ArrayList<UserDTO>() ;


    public List<UserDTO> getUserList() {
        return userList;
    }

    public void setUserList(List<UserDTO> userList) {
        this.userList = userList;
    }
}
