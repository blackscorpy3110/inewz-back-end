package com.higgsup.interestnews.backend.presentation.dto.category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class CategoryManageListDTO {
    List<CategoryDTO> categoryDTOs = new ArrayList<CategoryDTO>();

    public List<CategoryDTO> getCategoryDTOs() {
        return categoryDTOs;
    }

    public void setCategoryDTOs(List<CategoryDTO> categoryDTOs) {
        this.categoryDTOs = categoryDTOs;
    }
}
