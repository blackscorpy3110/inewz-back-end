package com.higgsup.interestnews.backend.presentation.dto.User;



/**
 * Created by Mr.Java on 1/13/2016.
 */
public class UserManageDTO {


    private Integer userId;

    private String fullName;

    private String userName;


    private String userEmail;


    private String userDescription;


    private String userAvatar;


    private String roleName;

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserManageDTO(Integer userId, String fullName, String userName, String userEmail, String userDescription, String userAvatar, String roleName) {
        this.userId = userId;
        this.fullName = fullName;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userDescription = userDescription;
        this.userAvatar = userAvatar;
        this.roleName = roleName;
    }

    public UserManageDTO() {
        super();
    }
}
