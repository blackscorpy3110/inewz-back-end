package com.higgsup.interestnews.backend.presentation.dto.post;

import com.higgsup.interestnews.backend.utils.GsonHelper;

import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ShowAllMyPublishedPost {

    private Integer postId;
    private String postTitle;
    private String postContent;
    private Date postDate;
    private String postImage;
    private Integer postVersion;
    private Integer catId;
    private String catName;
    private Integer userId;
    private String postStatus;
    private Integer tmpPostId;

    public ShowAllMyPublishedPost(Integer postId, String postTitle, String postContent,
                                  Date postDate, String postImage, Integer postVersion, Integer catId,
                                  String catName, Integer userId, String postStatus, Integer tmpPostId) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postImage = postImage;
        this.postVersion = postVersion;
        this.catId = catId;
        this.catName = catName;
        this.userId = userId;
        this.postStatus = postStatus;
        this.tmpPostId = tmpPostId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public Integer getPostVersion() {
        return postVersion;
    }

    public void setPostVersion(Integer postVersion) {
        this.postVersion = postVersion;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
