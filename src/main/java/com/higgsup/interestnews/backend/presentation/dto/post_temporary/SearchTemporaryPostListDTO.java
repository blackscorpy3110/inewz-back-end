package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class SearchTemporaryPostListDTO {
    List<SearchTemporaryPostDTO> searchTemporaryPostDTOs = new ArrayList<SearchTemporaryPostDTO>();

    public List<SearchTemporaryPostDTO> getSearchTemporaryPostDTOs() {
        return searchTemporaryPostDTOs;
    }

    public void setSearchTemporaryPostDTOs(List<SearchTemporaryPostDTO> searchTemporaryPostDTOs) {
        this.searchTemporaryPostDTOs = searchTemporaryPostDTOs;
    }
}
