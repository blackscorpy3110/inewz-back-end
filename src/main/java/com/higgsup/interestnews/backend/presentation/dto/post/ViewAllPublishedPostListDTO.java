package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ViewAllPublishedPostListDTO {
    List<ViewAllPublishedPostDTO> viewAllPublishedPostDTOList = new ArrayList<ViewAllPublishedPostDTO>();

    public List<ViewAllPublishedPostDTO> getViewAllPublishedPostDTOList() {
        return viewAllPublishedPostDTOList;
    }

    public void setViewAllPublishedPostDTOList(List<ViewAllPublishedPostDTO> viewAllPublishedPostDTOList) {
        this.viewAllPublishedPostDTOList = viewAllPublishedPostDTOList;
    }
}
