package com.higgsup.interestnews.backend.presentation.dto.post;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 2/25/2016.
 */
public class ViewMorePostListDTO {
    List<ViewMorePostDTO> viewMorePostDTOs = new ArrayList<ViewMorePostDTO>();

    public List<ViewMorePostDTO> getViewMorePostDTOs() {
        return viewMorePostDTOs;
    }

    public void setViewMorePostDTOs(List<ViewMorePostDTO> viewMorePostDTOs) {
        this.viewMorePostDTOs = viewMorePostDTOs;
    }
}
