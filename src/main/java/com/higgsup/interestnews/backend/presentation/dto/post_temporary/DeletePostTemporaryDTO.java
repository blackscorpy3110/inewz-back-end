package com.higgsup.interestnews.backend.presentation.dto.post_temporary;

import com.higgsup.interestnews.backend.utils.GsonHelper;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class DeletePostTemporaryDTO {
    private Integer tmpPostId;

    private String postTmpStatus;

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
