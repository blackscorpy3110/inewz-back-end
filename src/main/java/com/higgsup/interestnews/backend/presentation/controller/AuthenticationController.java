package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.AuthenticationDTO;
import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import com.higgsup.interestnews.backend.service.Authentication_service.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
public class AuthenticationController {

    @Autowired
    IAuthenticationService authenticationService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
//    @ResponseBody
    public AuthenticationDTO login(@RequestBody UserDTO user) throws Exception {
        return authenticationService.login(user);
    }

    @RequestMapping(path = "/checkAuthentication", method = RequestMethod.POST, produces = "application/text")
//    @ResponseBody
    public UserDTO checkAuthentication(HttpServletRequest request) throws Exception {
        return authenticationService.checkToken(request.getParameter("token"));
    }

//    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
//    @ResponseBody
//    public void uploadFile(
//            @RequestParam("uploadfile") MultipartFile uploadfile) {
//
//        try {
//            // Get the filename and build the local file path (be sure that the
//            // application have write permissions on such directory)
//            String filename = uploadfile.getOriginalFilename();
//            String directory = "C:/Users/Mr.Java/Desktop/uploadfile";
//            String filepath = Paths.get(directory, filename).toString();
//
//            // Save the file locally
//            BufferedOutputStream stream =
//                    new BufferedOutputStream(new FileOutputStream(new File(filepath)));
//            stream.write(uploadfile.getBytes());
//            stream.close();
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
////            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
//
////        return new ResponseEntity<>(HttpStatus.OK);
//    }
//        getImage getImage = new getImage(uploadfile);
//        getImage.saveImage(uploadfile, getImage.getFilePath());
//    }
}