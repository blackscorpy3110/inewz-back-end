package com.higgsup.interestnews.backend.presentation.dto.post;

import com.higgsup.interestnews.backend.utils.GsonHelper;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public class DeletePostDTO {
    private Integer tmpPostId;

    private Integer postId;

    public Integer getTmpPostId() {
        return tmpPostId;
    }

    public void setTmpPostId(Integer tmpPostId) {
        this.tmpPostId = tmpPostId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
