package com.higgsup.interestnews.backend.presentation.dto.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mr.Java on 1/13/2016.
 */
public class UserManageListDTO {
    public List<UserManageDTO> getUserManageDTOList() {
        return userManageDTOList;
    }

    public void setUserManageDTOList(List<UserManageDTO> userManageDTOList) {
        this.userManageDTOList = userManageDTOList;
    }

    private List<UserManageDTO> userManageDTOList = new ArrayList<UserManageDTO>();
}
