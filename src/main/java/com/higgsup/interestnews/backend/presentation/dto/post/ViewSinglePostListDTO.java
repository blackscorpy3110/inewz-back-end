package com.higgsup.interestnews.backend.presentation.dto.post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/19/2016.
 */
public class ViewSinglePostListDTO {
    List<ViewSinglePostDTO> viewSinglePostDTOs = new ArrayList<ViewSinglePostDTO>();

    public List<ViewSinglePostDTO> getViewSinglePostDTOs() {
        return viewSinglePostDTOs;
    }

    public void setViewSinglePostDTOs(List<ViewSinglePostDTO> viewSinglePostDTOs) {
        this.viewSinglePostDTOs = viewSinglePostDTOs;
    }
}
