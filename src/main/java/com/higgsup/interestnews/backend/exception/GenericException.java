package com.higgsup.interestnews.backend.exception;

import com.higgsup.interestnews.backend.common.i18nMessage;
import org.springframework.http.HttpStatus;

/**
 * Created by Mr.Java on 1/15/2016.
 */
public abstract class GenericException extends Exception {
    private String message;
    private HttpStatus httpStatusCode;
    private Throwable cause;
    private StackTraceElement[] stackTraceElements;

    public GenericException(i18nMessage message)
    {
        this(message.getBaseMessage(), message.getHttpStatus(), null);
    }

    public GenericException(i18nMessage message, Throwable cause)
    {
        this(message.getBaseMessage(), message.getHttpStatus(), cause);
    }

    private GenericException(String message, HttpStatus httpStatus, Throwable cause)
    {
        this.message = message;
        this.httpStatusCode = httpStatus;
        this.cause = cause;
        if (cause != null)
        {
            this.stackTraceElements = cause.getStackTrace();
        }
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatusCode;
    }

    public void setHttpStatus(HttpStatus httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }

    public StackTraceElement[] getStackTraceElements() {
        return stackTraceElements;
    }

    public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
        this.stackTraceElements = stackTraceElements;
    }
}
