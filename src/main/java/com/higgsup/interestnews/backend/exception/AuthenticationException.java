package com.higgsup.interestnews.backend.exception;

/**
 * Created by Bui Cong Thanh on 12/30/2015.
 */
public class AuthenticationException extends Exception {
    public AuthenticationException(String s) {
        super(s);
    }
}
