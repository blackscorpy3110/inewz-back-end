package com.higgsup.interestnews.backend.repository.custom;

import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public interface PostNativeRepository {
    public List<Object[]> showAllPublishedPost();

    public List<Object[]> showOnePublishedPost(Integer postId);

    public List<Object[]> showAllMyPublishedPost(Integer userId);

    public List<Object[]> showAllPostToHomePage();

    public List<Object[]> viewOnePublishedPost(Integer postId);

    public List<Object[]> morePostSameCategory(Integer postId,Integer catId);

    public List<Object[]> findPostByUserId(Integer userId);

    public List<Object[]> findPostByCategoryId(Integer catId);

    public boolean updateStatusWhenPublishedPostWasDeleted(Integer tmpPostId, Integer newPostId, Integer oldPostId, String status);


}
