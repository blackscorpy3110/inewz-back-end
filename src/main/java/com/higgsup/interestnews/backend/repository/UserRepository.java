package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import com.higgsup.interestnews.backend.repository.custom.CustomUserRepository;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer>, CustomUserRepository {
    public List<User> findByUserName(String userName);

    public List<User> findBySessionID(String token);

    public List<UserDTO> findByUserEmail(String userEmail);

    public List<UserDTO> findUserIdByUserName(String userName);



//    public List<User> findByUserNameOrEmail(String userName, String email);
}
