package com.higgsup.interestnews.backend.repository.custom;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public interface CatNativeRepository {
    public boolean editCatByCategoryId(Integer catId, String catName,String image, String catDescription);
}
