package com.higgsup.interestnews.backend.repository.impl;

import com.higgsup.interestnews.backend.repository.custom.PostTemporaryNativeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/11/2016.
 */
public class PostTemporaryRepositoryImpl implements PostTemporaryNativeRepository {

    @PersistenceContext
    EntityManager entityManager;


    //edit temporary post by tmp post id
    @Override
    public Boolean editPostById(Integer tmpPostId, Integer catId, String tmpPostTitle, String tmpPostContent, String tmpPostImage) {
        try {
            entityManager.createNativeQuery(
                    "UPDATE post_temporary " +
                            "SET category_id = ?,tmp_post_title = ?,tmp_post_content = ?,tmp_post_image = ? " +
                            "WHERE tmp_post_id = ?")
                    .setParameter(1, catId)
                    .setParameter(2, tmpPostTitle)
                    .setParameter(3, tmpPostContent)
                    .setParameter(4, tmpPostImage)
                    .setParameter(5, tmpPostId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //show all temporary post for manage
    @Override
    public List<Object[]> showAllTemporaryPost() {
        String sql = "SELECT p.tmp_post_id,u.full_name,c.category_id,c.category_name," +
                "p.post_id,p.tmp_post_title,p.tmp_post_content,p.tmp_post_date," +
                "p.tmp_post_image,p.tmp_post_status,p.tmp_post_version,p.user_id " +
                "FROM users AS u " +
                "INNER JOIN post_temporary AS p " +
                "ON u.user_id = p.user_id " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "ORDER BY p.tmp_post_id DESC";

        Query nativeQuery = entityManager.createNativeQuery(sql);

        return nativeQuery.getResultList();
    }

    //show one temporary
    @Override
    public List<Object[]> getOneTemporaryPost(Integer tmpPostId) {
        String sql = "SELECT p.tmp_post_id,u.full_name,c.category_id,c.category_name,p.post_id," +
                "p.tmp_post_title,p.tmp_post_content," +
                "p.tmp_post_date,p.tmp_post_image,p.tmp_post_status,p.tmp_post_version,p.user_id " +
                "FROM users AS u " +
                "INNER JOIN post_temporary AS p " +
                "ON u.user_id = p.user_id " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "WHERE p.tmp_post_id = ?";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, tmpPostId);
        return nativeQuery.getResultList();
    }

    @Override
    public List<Object[]> findAllMyTemporaryPost(Integer userId) {
        String sql = "SELECT p.tmp_post_id,c.category_name,p.tmp_post_title,p.tmp_post_content," +
                "p.tmp_post_date,p.tmp_post_image,p.tmp_post_status,tmp_post_version,p.user_id,p.category_id,p.post_id " +
                "FROM post_temporary AS p " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "WHERE p.user_id = ? " +
                "ORDER BY p.tmp_post_id DESC ";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, userId);
        return nativeQuery.getResultList();
    }

    @Override
    public boolean publishExistingPost(Integer tmpPostId, String postTitle, String postContent, Date date, String postImage, String postStatus, Integer postVersion, Integer postId) {
        try {
            entityManager.createNativeQuery(
                    "UPDATE posts SET tmp_post_id = ?,post_title = ?,post_content = ?,post_date = ?,post_image = ?,post_status = ?,post_version = ? " +
                            "WHERE post_id = ?")
                    .setParameter(1, tmpPostId)
                    .setParameter(2, postTitle)
                    .setParameter(3, postContent)
                    .setParameter(4, date)
                    .setParameter(5, postImage)
                    .setParameter(6, postStatus)
                    .setParameter(7, postVersion)
                    .setParameter(8, postId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean updateTmpPostWhenPublished(Integer tmpPostId, Integer postId, String tmpPostStatus) {
        try {
            entityManager.createNativeQuery(
                    "UPDATE post_temporary SET post_id = ?,tmp_post_status = ? WHERE tmp_post_id = ?")
                    .setParameter(1, postId)
                    .setParameter(2, tmpPostStatus)
                    .setParameter(3, tmpPostId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean updateStatusPostWhenPublishedNewVersion(Integer tmpPostId, Integer postId, String tmpPostStatus) {
        try {
            entityManager.createNativeQuery(
                    "UPDATE post_temporary SET tmp_post_status = ? WHERE post_id = ? AND tmp_post_id <> ? ")
                    .setParameter(1, tmpPostStatus)
                    .setParameter(2, postId)
                    .setParameter(3, tmpPostId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
