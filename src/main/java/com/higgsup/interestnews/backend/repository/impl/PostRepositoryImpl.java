package com.higgsup.interestnews.backend.repository.impl;

import com.higgsup.interestnews.backend.repository.custom.PostNativeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public class PostRepositoryImpl implements PostNativeRepository {
    @PersistenceContext
    EntityManager entityManager;

    //show post to home page
    @Override
    public List<Object[]> showAllPostToHomePage() {

        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,c.category_name,u.full_name,u.user_id,p.category_id " +
                "FROM users AS u " +
                "INNER JOIN posts AS p " +
                "ON  u.user_id = p.user_id " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "ORDER BY p.post_id DESC ";

        Query nativeQuery = entityManager.createNativeQuery(sql);

        return nativeQuery.getResultList();
    }

    //view single post
    @Override
    public List<Object[]> viewOnePublishedPost(Integer postId) {

        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,u.user_id,u.full_name,p.category_id " +
                "FROM posts AS p " +
                "INNER JOIN users AS u " +
                "ON p.user_id = u.user_id " +
                "WHERE p.post_id = ?";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, postId);

        return nativeQuery.getResultList();
    }

    @Override
    public List<Object[]> morePostSameCategory(Integer postId, Integer catId) {
        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,u.user_id,u.full_name,p.category_id " +
                "FROM posts AS p " +
                "INNER JOIN users AS u " +
                "ON p.user_id = u.user_id " +
                "WHERE p.category_id = ? " +
                "AND p.post_id <> ? " +
                "ORDER BY RAND() LIMIT 5";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, catId).setParameter(2,postId);

        return nativeQuery.getResultList();
    }

    //show all published post for manage
    @Override
    public List<Object[]> showAllPublishedPost() {
        String sql = "SELECT p.tmp_post_id,u.user_id,u.full_name,c.category_name,p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,p.post_status,p.post_version " +
                "FROM users u " +
                "INNER JOIN posts p " +
                "ON u.user_id = p.user_id " +
                "INNER JOIN categories c " +
                "ON p.category_id = c.category_id " +
                "ORDER BY p.post_id DESC";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        return nativeQuery.getResultList();
    }

    @Override
    public List<Object[]> showOnePublishedPost(Integer postId) {
        String sql = "SELECT p.user_id,p.post_id,p.category_id,p.post_title,p.post_content,c.category_name,p.post_image,p.post_version " +
                "FROM posts as p " +
                "INNER JOIN categories as c " +
                "ON p.category_id = c.category_id " +
                "WHERE p.post_id = ?";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, postId);
        return nativeQuery.getResultList();
    }

    //show all published post for person
    @Override
    public List<Object[]> showAllMyPublishedPost(Integer userId) {

        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,p.post_version,c.category_id,c.category_name,p.user_id,p.post_status,p.tmp_post_id " +
                "FROM posts AS p INNER JOIN categories AS c ON p.category_id = c.category_id " +
                "WHERE user_id = ? " +
                "ORDER BY p.post_id DESC";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, userId);

        return nativeQuery.getResultList();
    }

    @Override
    public List<Object[]> findPostByUserId(Integer userId) {
        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,c.category_id,c.category_name,u.full_name,u.user_id " +
                "FROM users AS u " +
                "INNER JOIN posts AS p " +
                "ON u.user_id = p.user_id " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "WHERE p.user_id = ? " +
                "ORDER BY p.post_id DESC ";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, userId);
        return nativeQuery.getResultList();
    }

    @Override
    public List<Object[]> findPostByCategoryId(Integer catId) {
        String sql = "SELECT p.post_id,p.post_title,p.post_content,p.post_date,p.post_image,c.category_id,c.category_name,u.full_name,u.user_id " +
                "FROM users AS u " +
                "INNER JOIN posts AS p " +
                "ON u.user_id = p.user_id " +
                "INNER JOIN categories AS c " +
                "ON p.category_id = c.category_id " +
                "WHERE p.category_id = ? " +
                "ORDER BY p.post_id DESC ";
        Query nativeQuery = entityManager.createNativeQuery(sql).setParameter(1, catId);
        return nativeQuery.getResultList();
    }

    @Override
    public boolean updateStatusWhenPublishedPostWasDeleted(Integer tmpPostId, Integer newPostId, Integer oldPostId, String status) {
        try {
            entityManager.createNativeQuery("UPDATE post_temporary SET tmp_post_status = ?,post_id = ? WHERE tmp_post_id = ? AND post_id = ?")
                    .setParameter(1, status)
                    .setParameter(2, newPostId)
                    .setParameter(3, tmpPostId)
                    .setParameter(4, oldPostId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
