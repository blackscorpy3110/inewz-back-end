package com.higgsup.interestnews.backend.repository.custom;

import java.util.List;

/**
 * Created by Mr.Java on 1/15/2016.
 */

public interface CustomUserRepository {
    List<Object[]> findAllUser();
}
