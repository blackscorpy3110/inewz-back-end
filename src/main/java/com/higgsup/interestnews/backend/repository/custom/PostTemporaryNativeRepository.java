package com.higgsup.interestnews.backend.repository.custom;

import java.util.Date;
import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/11/2016.
 */
public interface PostTemporaryNativeRepository {
    public Boolean editPostById(Integer tmpPostId, Integer catId, String tmpPostTitle, String tmpPostContent, String tmpPostImage);

    public List<Object[]> showAllTemporaryPost();

    public List<Object[]> getOneTemporaryPost(Integer tmpPostId);

    public List<Object[]> findAllMyTemporaryPost(Integer userId);

    public boolean publishExistingPost(Integer tmpPostId, String postTitle, String postContent, Date date, String postImage, String postStatus, Integer postVersion, Integer postId);

    public boolean updateTmpPostWhenPublished(Integer tmpPostId, Integer postId, String tmpPostStatus);

    public boolean updateStatusPostWhenPublishedNewVersion(Integer tmpPostId, Integer postId, String tmpPostStatus);

}
