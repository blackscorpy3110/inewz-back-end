package com.higgsup.interestnews.backend.repository.impl;

import com.higgsup.interestnews.backend.repository.custom.CustomUserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class UserRepositoryImpl implements CustomUserRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Object[]> findAllUser() {
        Query query = entityManager.createNativeQuery("SELECT u.user_id,u.full_name, u.user_name,u.user_email,u.user_description,u.user_avatar,r.role_name  " +
                "FROM users AS u " +
                "INNER JOIN role AS r " +
                "ON u.role_id = r.id ");
        return query.getResultList();
    }

//    @Override
//    public List<User> showAllUser() {
//       Query query = entityManager.createNativeQuery("SELECT u.user_id, u.user_name,u.user_avatar,u.user_description,u.user_email, r.role_name  " +
//                "FROM users AS u " +
//                "INNER JOIN role AS r " +
//                "ON u.role_id = r.id ");
//        return query.getResultList();
//
//    }


//        @Override
//         public List<User> findUserByUserName(String userName, RowMapper<UserDTO> rowMapper) {
//        return entityManager.createNativeQuery("SELECT * FROM users WHERE user_name = '?'",userName).getResultList();
//    }


//    @Override
//    public List<User> updateSessionIDByUserName(String sessionID, String userName) {
//        Query query = entityManager.createNamedQuery("SELECT * FROM something WHERE column = ?nameParam1");
//        query.setParameter("nameParam1", value)
//        return entityManager.createNativeQuery("UPDATE users SET session_id = '?' WHERE user_name = '?'").getResultList();
//    }


}
