package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.repository.custom.PostTemporaryNativeRepository;
import com.higgsup.interestnews.backend.service.model.PostTemporary;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Bui Cong Thanh on 1/9/2016.
 */
@Repository
public interface PostTemporaryRepository extends PagingAndSortingRepository<PostTemporary, Integer>, PostTemporaryNativeRepository {
}
