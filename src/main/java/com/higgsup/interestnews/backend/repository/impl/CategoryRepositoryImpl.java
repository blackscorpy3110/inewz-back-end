package com.higgsup.interestnews.backend.repository.impl;

import com.higgsup.interestnews.backend.repository.custom.CatNativeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public class CategoryRepositoryImpl implements CatNativeRepository {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public boolean editCatByCategoryId(Integer catId, String catName,String image, String catDescription) {
        try {
            entityManager.createNativeQuery("UPDATE categories SET category_name = ?,category_image = ?,category_description = ? WHERE category_id = ? ")
                    .setParameter(1, catName)
                    .setParameter(2, image)
                    .setParameter(3, catDescription)
                    .setParameter(4, catId)
                    .executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

}
