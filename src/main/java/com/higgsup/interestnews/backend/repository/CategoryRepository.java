package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.repository.custom.CatNativeRepository;
import com.higgsup.interestnews.backend.service.model.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category,Integer>, CatNativeRepository {
    @Query("SELECT c.categoryID,c.categoryName,c.categoryImage,c.categoryDes FROM Category AS c")
    public List<Object[]> showAllCategories();

    @Query("SELECT c.categoryID,c.categoryName,c.categoryImage,c.categoryDes FROM Category AS c WHERE c.categoryID = :catId")
    public List<Object[]> getOneCategory(@Param("catId") Integer catId);

    Integer countByCategoryName(String catName);
}
