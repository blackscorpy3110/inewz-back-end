package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.service.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Bui Cong Thanh on 12/31/2015.
 */
@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Integer> {
    public Role findIdByRoleName(String roleName);
}
