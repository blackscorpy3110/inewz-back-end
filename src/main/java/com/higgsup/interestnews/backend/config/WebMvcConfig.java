package com.higgsup.interestnews.backend.config;

import com.higgsup.interestnews.backend.presentation.interceptor.AuthenticationInterceptor;
import com.higgsup.interestnews.backend.presentation.interceptor.HelloWorldInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    HelloWorldInterceptor helloWorldInterceptor;

    @Autowired
    AuthenticationInterceptor authenticationInterceptor;

    @Override
    //dang ky interceptor
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor);
    }
}
