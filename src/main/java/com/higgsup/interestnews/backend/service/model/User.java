package com.higgsup.interestnews.backend.service.model;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by lent on 12/24/2015.
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "user_id", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "full_name", nullable = false)
    private String fullName;
    @Column(name = "user_name", nullable = false)
    private String userName; // user_name

    @Column(name = "user_email")
    private String userEmail;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "user_description", columnDefinition = "TEXT")
    private String userDescription;

    @Column(name = "user_avatar")
    private String userAvatar;

    @Column(name = "session_id")
    private String sessionID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getUserAvatar() {
        return this.userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", role=" + role +
                ", fullName=" + fullName +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", password='" + password + '\'' +
                ", userDescription='" + userDescription + '\'' +
                ", userAvatar='" + userAvatar + '\'' +
                ", sessionID='" + sessionID + '\'' +
                '}';
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
