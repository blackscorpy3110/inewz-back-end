package com.higgsup.interestnews.backend.service.user;

import com.higgsup.interestnews.backend.presentation.dto.User.*;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import org.hibernate.service.spi.ServiceException;

/**
 * Created by lent on 12/24/2015.
 */
public interface IUserService {

    SimpleDTO createUser(CreateRequestDTO createRequestDTO) throws ServiceException;
    UserManageListDTO getAllUser();

    SimpleDTO deleteUser(Integer userId);

    SimpleDTO updateUser(UpdateUserRequestDTO updateUserRequestDTO) throws ServiceException;
    boolean checkDuplicateUserName(String userName);
    boolean checkDuplicationUserEmail(String userEmail);
    UserManageListDTO getOneUser(UserManageDTO userManageDTO);
    String updateProfile(UpdateProfileRequestDTO updateProfileRequestDTO) throws ServiceException;

}
