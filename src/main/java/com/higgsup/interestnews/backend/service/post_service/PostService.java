package com.higgsup.interestnews.backend.service.post_service;

import com.google.gson.Gson;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post.*;
import com.higgsup.interestnews.backend.presentation.enums.PostStatus;
import com.higgsup.interestnews.backend.repository.CategoryRepository;
import com.higgsup.interestnews.backend.repository.PostRepository;
import com.higgsup.interestnews.backend.repository.PostTemporaryRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.Category;
import com.higgsup.interestnews.backend.service.model.PostTemporary;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Service
@Transactional(readOnly = false)
public class PostService implements IPostService {
    @Autowired
    PostRepository postRepository;

    @Autowired
    PostTemporaryRepository postTemporaryRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    //show post info to manager (done)
    @Transactional(readOnly = true)
    @Override
    public ShowAllPublishedPostListDTO showAllPublishedPost() {

        ShowAllPublishedPostListDTO postManageListDTO = new ShowAllPublishedPostListDTO();

        List<Object[]> objects = postRepository.showAllPublishedPost();

        for (Object[] post : objects) {
            ShowAllPublishedPostDTO showAllPublishedPostDTO = new ShowAllPublishedPostDTO((Integer) post[0],
                    (Integer) post[1], (String) post[2], (String) post[3],
                    (Integer) post[4], (String) post[5], (String) post[6],
                    (Date) post[7], (String) post[8], (String) post[9], (Integer) post[10]);

            postManageListDTO.getPublishedPostDTOList().add(showAllPublishedPostDTO);
        }
        return postManageListDTO;
    }

    //show one post for edit
    @Transactional(readOnly = true)
    @Override
    public ShowOnePostListDTO showOnePublishedPost(Integer tmpPostId) {

        ShowOnePostListDTO onePostListDTO = new ShowOnePostListDTO();

        List<Object[]> objects = postRepository.showOnePublishedPost(tmpPostId);

        for (Object[] post : objects) {
            ShowOnePostDTO showOnePostDTO = new ShowOnePostDTO(
                    (Integer) post[0], (Integer) post[1], (Integer) post[2],
                    (String) post[3], (String) post[4], (String) post[5],
                    (String) post[6], (Integer) post[7]);

            onePostListDTO.getShowOnePostDTOs().add(showOnePostDTO);
        }
        return onePostListDTO;
    }

    //show post of some one to them
    @Transactional(readOnly = true)
    @Override
    public ShowAllMyPublishedPostListDTO showAllMyPublishedPost(String tokenInput) {

        ShowAllMyPublishedPostListDTO postManageListDTO = new ShowAllMyPublishedPostListDTO();

        List<User> result = userRepository.findBySessionID(tokenInput);
        User user = result.get(0);


        List<Object[]> objects = postRepository.showAllMyPublishedPost(user.getId());

        for (Object[] post : objects) {
            ShowAllMyPublishedPost myPost = new ShowAllMyPublishedPost(
                    (Integer) post[0], (String) post[1], (String) post[2],
                    (Date) post[3], (String) post[4], (Integer) post[5], (Integer) post[6],
                    (String) post[7], (Integer) post[8], (String) post[9], (Integer) post[10]);

            postManageListDTO.getPublishedPosts().add(myPost);
        }
        return postManageListDTO;
    }

    //edit published post and insert to post temporary
    @Override
    public SimpleDTO editPublishedPost(String field, MultipartFile file) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();
        Gson gson = new Gson();
        UpdatePostDTO updatePostDTO = gson.fromJson(field, UpdatePostDTO.class);

        PostTemporary postTemporary = new PostTemporary();

        if (!file.isEmpty()) {
            try {
                String originalFilename = file.getOriginalFilename();
                String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
                UUID uuid = UUID.randomUUID();
                String filename = uuid.toString() + fileExtension;
                String directory = "D:/XAMPP/htdocs/interestnews/front-end/src/main/resources/static/customDesign/images/post";
                String filepath = Paths.get(directory, filename).toString();

                // Save the file locally
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                stream.write(file.getBytes());
                stream.close();

                postTemporary.setPostId(updatePostDTO.getPostId());
                postTemporary.setPostTmpTitle(updatePostDTO.getPostTitle());
                postTemporary.setPostTmpContent(updatePostDTO.getPostContent());
                postTemporary.setPostTmpDate(new Date());
                postTemporary.setPostTmpImage(filename);
                postTemporary.setPostTmpStatus(PostStatus.PENDING.getString());
                postTemporary.setPostTmpVersion(updatePostDTO.getPostVersion() + 1);

                User user = userRepository.findOne(updatePostDTO.getUserId());
                postTemporary.setUserId(user);

                Category category = categoryRepository.findOne(updatePostDTO.getCatId());
                postTemporary.setCategoryId(category);

                postTemporaryRepository.save(postTemporary);
                simpleDTO.setMessage("Sửa thành công,phiên bản mới của bài viết đã được gửi vào danh sách chờ duyệt.");
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            throw new Exception("Chưa có ảnh của bài viết.");
        }

        return simpleDTO;
    }

    //find by user id
    @Transactional(readOnly = true)
    @Override
    public SearchPostListDTO searchPostByUserId(Integer userId) {
        SearchPostListDTO searchPostListDTO = new SearchPostListDTO();

        List<Object[]> objects = postRepository.findPostByUserId(userId);

        for (Object[] post : objects) {
            SearchPostDTO searchPostDTO = new SearchPostDTO(
                    (Integer) post[0], (String) post[1], (String) post[2], (Date) post[3],
                    (String) post[4], (Integer) post[5], (String) post[6], (String) post[7], (Integer) post[8]);

            searchPostListDTO.getSearchPostDTOs().add(searchPostDTO);
        }
        return searchPostListDTO;
    }

    //find by cat id
    @Transactional(readOnly = true)
    @Override
    public SearchPostListDTO searchPostByCategoryId(Integer catId) {
        SearchPostListDTO searchPostListDTO = new SearchPostListDTO();

        List<Object[]> objects = postRepository.findPostByCategoryId(catId);

        for (Object[] post : objects) {
            SearchPostDTO searchPostDTO = new SearchPostDTO(
                    (Integer) post[0], (String) post[1], (String) post[2], (Date) post[3],
                    (String) post[4], (Integer) post[5], (String) post[6], (String) post[7], (Integer) post[8]);

            searchPostListDTO.getSearchPostDTOs().add(searchPostDTO);
        }
        return searchPostListDTO;
    }

    //delete published post
    @Override
    public SimpleDTO deletePublishedPost(DeletePostDTO deletePostDTO) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();
        //delete by post id
        postRepository.delete(deletePostDTO.getPostId());

        //when delete done,set new postId to oldpostid at post temporary and set deleted status
        if (postRepository.findOne(deletePostDTO.getPostId()) == null) {

            Integer tmpPostId = deletePostDTO.getTmpPostId();
            Integer oldpostId = deletePostDTO.getPostId();
            Integer newPostId = 0;
            String tmpStatus = PostStatus.DELETED.getString();

            if (postRepository.updateStatusWhenPublishedPostWasDeleted(tmpPostId, newPostId, oldpostId, tmpStatus) == true) {
                simpleDTO.setMessage("Bài viết đã được xóa thành công!");
            }

        } else {
            throw new Exception("Xóa bài viết thất bại!");
        }
        return simpleDTO;
    }

    //show post info to index (done)
    @Transactional(readOnly = true)
    @Override
    public ViewAllPublishedPostListDTO viewAllPostInHomePage() {

        ViewAllPublishedPostListDTO postManageListDTO = new ViewAllPublishedPostListDTO();

        List<Object[]> objects = postRepository.showAllPostToHomePage();

        for (Object[] post : objects) {
            ViewAllPublishedPostDTO viewAll = new ViewAllPublishedPostDTO(
                    (Integer) post[0], (String) post[1], (String) post[2], (Date) post[3],
                    (String) post[4], (String) post[5], (String) post[6], (Integer) post[7], (Integer) post[8]);

            postManageListDTO.getViewAllPublishedPostDTOList().add(viewAll);
        }

        return postManageListDTO;
    }

    //view one published post
    @Transactional(readOnly = true)
    @Override
    public ViewSinglePostListDTO viewSinglePostById(Integer postId) {

        ViewSinglePostListDTO postManageListDTO = new ViewSinglePostListDTO();

        List<Object[]> objects = postRepository.viewOnePublishedPost(postId);

        for (Object[] post : objects) {
            ViewSinglePostDTO singlePost = new ViewSinglePostDTO(
                    (Integer) post[0], (String) post[1], (String) post[2],
                    (Date) post[3], (String) post[4], (Integer) post[5], (String) post[6], (Integer) post[7]);

            postManageListDTO.getViewSinglePostDTOs().add(singlePost);
        }
        return postManageListDTO;
    }

    @Override
    public ViewMorePostListDTO morePostSameCategory(ViewMorePostDTO viewMorePostDTO) {
        ViewMorePostListDTO viewMorePostListDTO = new ViewMorePostListDTO();

        List<Object[]> objects = postRepository.morePostSameCategory(viewMorePostDTO.getPostId(),viewMorePostDTO.getCatId());

        for (Object[] post : objects) {
            ViewMorePostDTO viewMorePostDTO1 = new ViewMorePostDTO(
                    (Integer) post[0], (String) post[1], (String) post[2],
                    (Date) post[3], (String) post[4], (Integer) post[5], (String) post[6], (Integer) post[7]);

            viewMorePostListDTO.getViewMorePostDTOs().add(viewMorePostDTO1);
        }
        return viewMorePostListDTO;
    }
}
