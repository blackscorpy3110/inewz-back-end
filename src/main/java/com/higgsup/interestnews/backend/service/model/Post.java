package com.higgsup.interestnews.backend.service.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Entity
@Table(name = "posts")
public class Post implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "post_id")
    private int postId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "tmp_post_id", nullable = false)
    private int postTmpID;

    @Column(name = "post_title", nullable = false)
    private String postTitle;

    @Column(name = "post_content", nullable = false, columnDefinition = "TEXT")
    private String postContent;

    @Column(name = "post_date", nullable = false)
    private Date postDate;

    @Column(name = "post_image", nullable = false)
    private String postImage;

    @Column(name = "post_status", nullable = false)
    private String postStatus;

    @Column(name = "post_version", nullable = false)
    private Integer postVersion;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public User getUserId() {
        return user;
    }

    public void setUserId(User userId) {
        this.user = userId;
    }

    public Category getCategoryId() {
        return category;
    }

    public void setCategoryId(Category categoryId) {
        this.category = categoryId;
    }

    public int getPostTmpID() {
        return postTmpID;
    }

    public void setPostTmpID(int postTmpID) {
        this.postTmpID = postTmpID;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public Integer getPostVersion() {
        return postVersion;
    }

    public void setPostVersion(Integer postVersion) {
        this.postVersion = postVersion;
    }
}
