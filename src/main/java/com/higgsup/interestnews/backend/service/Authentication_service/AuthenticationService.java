package com.higgsup.interestnews.backend.service.Authentication_service;

import com.higgsup.interestnews.backend.presentation.dto.AuthenticationDTO;
import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import com.higgsup.interestnews.backend.repository.RoleRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.User;
import com.higgsup.interestnews.backend.utils.MD5utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class AuthenticationService implements IAuthenticationService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;


    @Override
    @Transactional(readOnly = false)//cho phep ghi
    public AuthenticationDTO login(UserDTO user) throws Exception {
        List<User> result = userRepository.findByUserName(user.getUsername());
        if(result.size() == 0){
            throw new Exception("user doesn't exits!");
        }

        UserDTO dto = new UserDTO();
        for (User user1 : result) {
            dto.setFullName(user1.getFullName());
            dto.setUsername(user1.getUserName());
            dto.setPassword(user1.getPassword());
            dto.setUserEmail(user1.getUserEmail());
            dto.setRoleID(user1.getRole());
            dto.setSessionID(user1.getSessionID());
            dto.setUserDescription(user1.getUserDescription());
            dto.setUserAvatar(user1.getUserAvatar());
            dto.setUserId(user1.getId());
        }

        if (!dto.getPassword().equals(MD5utils.md5(user.getPassword()))) {
            throw new Exception("Password is wrong !");
        }


        dto.setSessionID(UUID.randomUUID().toString());
        User user1 = result.get(0);
        user1.setSessionID(dto.getSessionID());
        userRepository.save(user1);

        AuthenticationDTO authenticationDTO = new AuthenticationDTO();
        authenticationDTO.setToken(dto.getSessionID());
        authenticationDTO.setRole(dto.getRoleID().getId());
        authenticationDTO.setFullName(dto.getFullName());
        return authenticationDTO;
    }

    @Override
    public UserDTO checkToken(String token) throws Exception {
        List<User> result = userRepository.findBySessionID(token);
        UserDTO dto = new UserDTO();
        for (User user1 : result) {
            dto.setRoleID(user1.getRole());
            dto.setSessionID(user1.getSessionID());
        }
        return dto;
    }

}
