package com.higgsup.interestnews.backend.service.post_temporary_service;

import com.google.gson.Gson;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post_temporary.*;
import com.higgsup.interestnews.backend.presentation.enums.PostStatus;
import com.higgsup.interestnews.backend.repository.CategoryRepository;
import com.higgsup.interestnews.backend.repository.PostRepository;
import com.higgsup.interestnews.backend.repository.PostTemporaryRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.Category;
import com.higgsup.interestnews.backend.service.model.Post;
import com.higgsup.interestnews.backend.service.model.PostTemporary;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
@Service
@Transactional(readOnly = false)
public class PostTemporaryService implements IPostTemporaryService {
    // I or no ?
    @Autowired
    PostTemporaryRepository postTemporaryRepository;

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    //create new post to post temporary (done)
    @Override
    public SimpleDTO createNewPost(String token,String field, MultipartFile file) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();
        Gson gson = new Gson();
        CreatePostTemporaryDTO createPostTemporaryDTO = gson.fromJson(field, CreatePostTemporaryDTO.class);

        List<User> result = userRepository.findBySessionID(token);
        User author = result.get(0);

        PostTemporary postTemporary = new PostTemporary();


        if (!file.isEmpty()) {
            try {
                String originalFilename = file.getOriginalFilename();
                String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
                UUID uuid = UUID.randomUUID();
                String filename = uuid.toString() + fileExtension;
                String directory = "D:/XAMPP/htdocs/interestnews/front-end/src/main/resources/static/customDesign/images/post";
                String filepath = Paths.get(directory, filename).toString();

                // Save the file locally
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                stream.write(file.getBytes());
                stream.close();

                postTemporary.setPostId(0);
                postTemporary.setPostTmpTitle(createPostTemporaryDTO.getTmpPostTitle());
                postTemporary.setPostTmpContent(createPostTemporaryDTO.getTmpPostContent());
                postTemporary.setPostTmpDate(new Date());

                postTemporary.setPostTmpStatus(PostStatus.PENDING.getString());
                postTemporary.setPostTmpVersion(1);

                User user = userRepository.findOne(author.getId());
                postTemporary.setUserId(user);

                Category category = categoryRepository.findOne(createPostTemporaryDTO.getCategoryId());
                postTemporary.setCategoryId(category);

                postTemporary.setPostTmpImage(filename);

                postTemporaryRepository.save(postTemporary);
                simpleDTO.setMessage("Bài viết đã được gửi thành công và đang trong trạng thái chờ duyệt !");

            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            throw new Exception("Chưa có ảnh của bài viết");
        }
        return simpleDTO;
    }

    //edit temporary post by tmp post id (done)
    @Override
    public SimpleDTO updateTemporaryPost(String field, MultipartFile file) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();

        Gson gson = new Gson();
        UpdatePostTemporaryDTO updatePostTemporaryDTO = gson.fromJson(field, UpdatePostTemporaryDTO.class);
            try {
                String originalFilename = file.getOriginalFilename();
                String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
                UUID uuid = UUID.randomUUID();
                String filename = uuid.toString() + fileExtension;
                String directory = "D:/XAMPP/htdocs/interestnews/front-end/src/main/resources/static/customDesign/images/post";
                String filepath = Paths.get(directory, filename).toString();

                // Save the file locally
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                stream.write(file.getBytes());
                stream.close();

                //1: get info form client
                Integer tmpId = updatePostTemporaryDTO.getTmpPostId();
                Integer catId = updatePostTemporaryDTO.getCatId();
                String tit = updatePostTemporaryDTO.getTmpPostTitle();
                String con = updatePostTemporaryDTO.getTmpPostContent();
                String img = filename;

                if (postTemporaryRepository.editPostById(tmpId, catId, tit, con, img) == true) {

                    simpleDTO.setMessage("Sửa bài viết thành công !");
                } else {
                    throw new Exception("Sửa bài viết thất bại");
                }
            } catch (Exception e) {
                e.getMessage();
            }

        return simpleDTO;
    }

    //show all temporary post to manager (done)
    @Transactional(readOnly = true)
    @Override
    public ShowAllTemporaryPostListDTO showAllTemporaryPost() {

        ShowAllTemporaryPostListDTO allTemporaryPostListDTO = new ShowAllTemporaryPostListDTO();

        List<Object[]> properties = postTemporaryRepository.showAllTemporaryPost();

        for (Object[] post : properties) {

            ShowAllTemporaryPostDTO postDTO = new ShowAllTemporaryPostDTO(
                    ((Integer) post[0]), ((String) post[1]), ((Integer) post[2]),
                    ((String) post[3]), ((Integer) post[4]), ((String) post[5]),
                    ((String) post[6]), ((Date) post[7]), ((String) post[8]),
                    ((String) post[9]), ((Integer) post[10]), ((Integer) post[11]));

            allTemporaryPostListDTO.getAllTemporaryPostDTOs().add(postDTO);

        }
        return allTemporaryPostListDTO;
    }

    //delete temporary post by id (done)
    @Override
    public SimpleDTO deleteTemporaryPost(DeletePostTemporaryDTO deletePostTemporaryDTO) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();
        if (deletePostTemporaryDTO.getPostTmpStatus().equals("PENDING")) {
            postTemporaryRepository.delete(deletePostTemporaryDTO.getTmpPostId());
        } else {
            simpleDTO.setMessage("Bài viết đang trong trạng thái kích hoạt,không thể xóa trong bảng chờ duyệt !");
        }

        if (postTemporaryRepository.findOne(deletePostTemporaryDTO.getTmpPostId()) == null) {
            simpleDTO.setMessage("Xóa bài viết thành công!");
        } else {
            throw new Exception("Đã xảy ra lỗi .");
        }

        return simpleDTO;
    }


    //get one temporary post for manage (done)
    @Transactional(readOnly = true)
    @Override
    public ShowAllTemporaryPostListDTO getOneTemporaryPostById(Integer tmpPostId) {

        ShowAllTemporaryPostListDTO showAllTemporaryPostListDTO = new ShowAllTemporaryPostListDTO();


        List<Object[]> tmpProperties = postTemporaryRepository.getOneTemporaryPost(tmpPostId);

        for (Object[] postTmp : tmpProperties) {

            ShowAllTemporaryPostDTO postDTO = new ShowAllTemporaryPostDTO(
                    ((Integer) postTmp[0]), ((String) postTmp[1]), ((Integer) postTmp[2]),
                    ((String) postTmp[3]), ((Integer) postTmp[4]), ((String) postTmp[5]),
                    ((String) postTmp[6]), ((Date) postTmp[7]), ((String) postTmp[8]),
                    ((String) postTmp[9]), ((Integer) postTmp[10]), ((Integer) postTmp[11]));

            showAllTemporaryPostListDTO.getAllTemporaryPostDTOs().add(postDTO);
        }
        return showAllTemporaryPostListDTO;
    }


    //show all temporary post of them
    @Transactional(readOnly = true)
    @Override
    public SearchTemporaryPostListDTO showAllMyTemporaryPost(String token) {
        SearchTemporaryPostListDTO searchTemporaryPostListDTO = new SearchTemporaryPostListDTO();

        List<User> result = userRepository.findBySessionID(token);
        User user = result.get(0);

        List<Object[]> myTmpProperties = postTemporaryRepository.findAllMyTemporaryPost(user.getId());

        for (Object[] myTmp : myTmpProperties) {
            SearchTemporaryPostDTO searchTemporaryPostDTO1 = new SearchTemporaryPostDTO(
                    ((Integer) myTmp[0]), ((String) myTmp[1]), ((String) myTmp[2]),
                    ((String) myTmp[3]), ((Date) myTmp[4]), ((String) myTmp[5]),
                    ((String) myTmp[6]), ((Integer) myTmp[7]), ((Integer) myTmp[8]), ((Integer) myTmp[9]), ((Integer) myTmp[10]));

            searchTemporaryPostListDTO.getSearchTemporaryPostDTOs().add(searchTemporaryPostDTO1);

        }
        return searchTemporaryPostListDTO;
    }


    //publish post (done)
    @Override
    public SimpleDTO publishTemporaryPost(PublishPostTemporaryDTO publishPostTemporaryDTO) throws Exception {

        SimpleDTO simpleDTO = new SimpleDTO();

        PostTemporaryDTO postTemporaryDTO = new PostTemporaryDTO();

        //1: if this is temporary post's first version,insert new post
        if (publishPostTemporaryDTO.getPostId() == 0) {
            Post post = new Post();
            post.setPostTmpID(publishPostTemporaryDTO.getTmpPostId());
            post.setPostTitle(publishPostTemporaryDTO.getTmpPostTitle());
            post.setPostContent(publishPostTemporaryDTO.getTmpPostContent());
            post.setPostDate(publishPostTemporaryDTO.getTmpPostDate());
            post.setPostImage(publishPostTemporaryDTO.getTmpPostImage());
            post.setPostStatus(PostStatus.PUBLISHED.getString());
            post.setPostVersion(1);

            User user = userRepository.findOne(publishPostTemporaryDTO.getUserId());
            post.setUserId(user);

            Category category = categoryRepository.findOne(publishPostTemporaryDTO.getCatId());
            post.setCategoryId(category);

            postRepository.save(post);

            //2: update post id and tmp post stt when publish done
            Integer postId = post.getPostId();
            Integer tmpPostId = post.getPostTmpID();
            String postStatus = PostStatus.ONLINE.getString();

            if (postTemporaryRepository.updateTmpPostWhenPublished(tmpPostId, postId, postStatus) == true) {

                simpleDTO.setMessage("Bài viết đã được duyệt thành công !");
            } else {
                throw new Exception("Duyệt bài viết thất bại !");
            }

            return simpleDTO;
        }
        //3: this temporary post existing -> update it by post_id
        if (publishPostTemporaryDTO.getPostId() > 0 && !publishPostTemporaryDTO.getPostTmpStatus().equals("ONLINE")) {

            // 4: update to post table
            int tmpId = publishPostTemporaryDTO.getTmpPostId();
            String tit = publishPostTemporaryDTO.getTmpPostTitle();
            String con = publishPostTemporaryDTO.getTmpPostContent();
            Date d = publishPostTemporaryDTO.getTmpPostDate();
            String img = publishPostTemporaryDTO.getTmpPostImage();
            String stt = PostStatus.PUBLISHED.getString();
            Integer ver = publishPostTemporaryDTO.getTmpPostVersion();
            Integer pId = publishPostTemporaryDTO.getPostId();

            if (postTemporaryRepository.publishExistingPost(tmpId, tit, con, d, img, stt, ver, pId) == true) {

                //5: set online for post was published in post temporary
                if (postTemporaryRepository.updateTmpPostWhenPublished(publishPostTemporaryDTO.getTmpPostId(), publishPostTemporaryDTO.getPostId(), PostStatus.ONLINE.getString()) == true) {

                    //6: and set pending for all old post with same post_id in post temporary
                    postTemporaryRepository.updateStatusPostWhenPublishedNewVersion(tmpId, pId, PostStatus.PENDING.getString());

                } else {
                    throw new Exception("Trạng thái của bài viết trong bảng tạm chưa được thay đổi !");
                }
            } else {
                throw new Exception("Duyệt bài viết thất bại !");
            }
        } else {
            throw new Exception("Bài viết này đang trong trại thái kích hoạt rồi");
        }
        return simpleDTO;
    }
}
