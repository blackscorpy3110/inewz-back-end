package com.higgsup.interestnews.backend.service.post_temporary_service;

import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post_temporary.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Bui Cong Thanh on 1/13/2016.
 */
public interface IPostTemporaryService {
    public SimpleDTO createNewPost(String token,String field, MultipartFile file) throws Exception;

    public SimpleDTO updateTemporaryPost(String field, MultipartFile file) throws Exception;

    public ShowAllTemporaryPostListDTO showAllTemporaryPost();

    public SimpleDTO deleteTemporaryPost(DeletePostTemporaryDTO deletePostTemporaryDTO) throws Exception;

    public ShowAllTemporaryPostListDTO getOneTemporaryPostById(Integer tmpPostId);

    public SearchTemporaryPostListDTO showAllMyTemporaryPost(String token);

    public SimpleDTO publishTemporaryPost(PublishPostTemporaryDTO publishPostTemporaryDTO) throws Exception;
}
