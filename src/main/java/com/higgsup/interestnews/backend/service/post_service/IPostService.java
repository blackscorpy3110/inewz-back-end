package com.higgsup.interestnews.backend.service.post_service;

import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.presentation.dto.post.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
public interface IPostService {
    public ViewAllPublishedPostListDTO viewAllPostInHomePage();

    public ShowAllPublishedPostListDTO showAllPublishedPost();

    public ShowAllMyPublishedPostListDTO showAllMyPublishedPost(String tokenInput);

    public ShowOnePostListDTO showOnePublishedPost(Integer tmpPostId);

    public ViewSinglePostListDTO viewSinglePostById(Integer postId);

    public ViewMorePostListDTO morePostSameCategory(ViewMorePostDTO viewMorePostDTO);

    public SimpleDTO deletePublishedPost(DeletePostDTO deletePostDTO) throws Exception;

    public SimpleDTO editPublishedPost(String field, MultipartFile file) throws Exception;

    public SearchPostListDTO searchPostByUserId(Integer userId);

    public SearchPostListDTO searchPostByCategoryId(Integer catId);
}
