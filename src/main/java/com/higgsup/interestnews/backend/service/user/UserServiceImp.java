package com.higgsup.interestnews.backend.service.user;

import com.higgsup.interestnews.backend.presentation.dto.User.*;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.repository.RoleRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.model.User;
import com.higgsup.interestnews.backend.utils.MD5utils;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(readOnly = true)//k cho ghi du lieu vao db
public class UserServiceImp implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Transactional(readOnly = false)
    @Override
    public SimpleDTO createUser(CreateRequestDTO createRequestDTO) throws ServiceException {
        SimpleDTO simpleDTO = new SimpleDTO();
        if (checkDuplicateUserName(createRequestDTO.getUserName()) == false) {
            simpleDTO.setMessage("username is already existed");
        } else if (checkDuplicationUserEmail(createRequestDTO.getUserEmail()) == false) {
            simpleDTO.setMessage("Email is already existed ");
        } else {
            String fileName = "no_avatar.jpg";
            User user = new User();
            user.setRole(roleRepository.findOne(4));
            user.setFullName(createRequestDTO.getFullName());
            user.setUserName(createRequestDTO.getUserName());
            user.setPassword(MD5utils.md5(createRequestDTO.getUserPassword()));
            user.setUserEmail(createRequestDTO.getUserEmail());
            user.setUserDescription(createRequestDTO.getUserDescription());
            user.setUserAvatar(fileName);
            userRepository.save(user);
            simpleDTO.setMessage("You have registered successfully...!");
        }
        return simpleDTO;
    }


    @Override
    public UserManageListDTO getAllUser() {
        UserManageListDTO userManageDTOList = new UserManageListDTO();
        List<Object[]> userProperties = userRepository.findAllUser();
        for (Object[] user : userProperties) {
            UserManageDTO userManageDTO = new UserManageDTO();
            userManageDTO.setUserId((Integer) user[0]);
            userManageDTO.setFullName((String) user[1]);
            userManageDTO.setUserName((String) user[2]);
            userManageDTO.setUserEmail((String) user[3]);
            userManageDTO.setUserDescription((String) user[4]);
            userManageDTO.setUserAvatar((String) user[5]);
            userManageDTO.setRoleName((String) user[6]);
            userManageDTOList.getUserManageDTOList().add(userManageDTO);
        }
        return userManageDTOList;
    }

    @Transactional(readOnly = false)
    @Override
    public SimpleDTO updateUser(UpdateUserRequestDTO updateUserRequestDTO) throws ServiceException {
//        if(userRepository.findOne(updateUserRequestDTO.getUserId())!=null){
//            return "Not found...!";
//        }else{
        SimpleDTO simpleDTO = new SimpleDTO();
        User user = userRepository.findOne(updateUserRequestDTO.getUserId());
        user.setRole(roleRepository.findIdByRoleName(updateUserRequestDTO.getRoleName()));
        user.setFullName(updateUserRequestDTO.getFullName());
        // xu ly username
        if (user.getUserName().equals(updateUserRequestDTO.getUserName())) {
            user.setUserName(user.getUserName());
        } else if (checkDuplicateUserName(updateUserRequestDTO.getUserName()) == false) {
            simpleDTO.setMessage("Username is already existed!");
        } else {
            user.setUserName(updateUserRequestDTO.getUserName());
        }
        user.setPassword(user.getPassword());

        //xu ly email
        if (user.getUserEmail().equals(updateUserRequestDTO.getUserEmail())) {
            user.setUserEmail(user.getUserEmail());
        } else if (checkDuplicationUserEmail(updateUserRequestDTO.getUserEmail()) == false) {
            simpleDTO.setMessage("Email is already existed!");
        } else {
            user.setUserEmail(updateUserRequestDTO.getUserEmail());
        }
        user.setUserDescription(updateUserRequestDTO.getUserDescription());
        //handling image
        //=> create font-end
        user.setUserAvatar(updateUserRequestDTO.getUserAvatar());
        userRepository.save(user);
        simpleDTO.setMessage("You have updated successfully...!");
        return simpleDTO;
    }

    @Override
    public boolean checkDuplicateUserName(String userName) {
        if (userRepository.findByUserName(userName).size() != 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean checkDuplicationUserEmail(String userEmail) {
        if (userRepository.findByUserEmail(userEmail).size() != 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public UserManageListDTO getOneUser(UserManageDTO userManageDTO) {
        UserManageListDTO userManageListDTO = new UserManageListDTO();
        User user = userRepository.findOne(userManageDTO.getUserId());
//        List<User> users = new ArrayList<>();
//        users.add(user);
//        for (User user1 : users) {
        UserManageDTO dto = new UserManageDTO();
        dto.setUserId(user.getId());
        dto.setRoleName(roleRepository.findOne(user.getRole().getId()).getRoleName());
        dto.setFullName(user.getFullName());
        dto.setUserName(user.getUserName());
        dto.setUserEmail(user.getUserEmail());
        dto.setUserDescription(user.getUserDescription());
        dto.setUserAvatar(user.getUserAvatar());
        userManageListDTO.getUserManageDTOList().add(dto);
//        }
        return userManageListDTO;
    }

    @Override
    public String updateProfile(UpdateProfileRequestDTO updateProfileRequestDTO) throws ServiceException {
        User user = userRepository.findOne(updateProfileRequestDTO.getUserId());
        user.setFullName(updateProfileRequestDTO.getFullName());
        // xu ly username
        if (user.getUserName().equals(updateProfileRequestDTO.getUserName())) {
            user.setUserName(user.getUserName());
        } else if (checkDuplicateUserName(updateProfileRequestDTO.getUserName()) == false) {
            return "username is already existed";
        } else {
            user.setUserName(updateProfileRequestDTO.getUserName());
        }
        user.setPassword(MD5utils.md5(updateProfileRequestDTO.getUserPassword()));

        //xu ly email
        if (user.getUserEmail().equals(updateProfileRequestDTO.getUserEmail())) {
            user.setUserEmail(user.getUserEmail());
        } else if (checkDuplicationUserEmail(updateProfileRequestDTO.getUserEmail()) == false) {
            return "Email is already existed ";
        } else {
            user.setUserEmail(updateProfileRequestDTO.getUserEmail());
        }
        user.setUserDescription(updateProfileRequestDTO.getUserDescription());
        user.setUserAvatar(updateProfileRequestDTO.getUserAvatar());
        userRepository.save(user);
        return "You have updated successfully...!";
    }

    @Transactional(readOnly = false)
    @Override
    public SimpleDTO deleteUser(Integer userId) {
        SimpleDTO simpleDTO = new SimpleDTO();
        if (userRepository.findOne(userId) != null) {
            userRepository.delete(userId);

            simpleDTO.setMessage("You have successfully removed..!");
        } else {
            simpleDTO.setMessage("Can not removed...!");
        }
        return simpleDTO;
    }
}
