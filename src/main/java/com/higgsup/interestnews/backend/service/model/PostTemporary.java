package com.higgsup.interestnews.backend.service.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Bui Cong Thanh on 1/9/2016.
 */
@Entity
@Table(name = "post_temporary")
public class PostTemporary {
    @Id
    @GeneratedValue
    @Column(name = "tmp_post_id", nullable = false)
    private Integer postTmpID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "post_id", nullable = false)
    private int postId;

    @Column(name = "tmp_post_title", nullable = false)
    private String postTmpTitle;

    @Column(name = "tmp_post_content", nullable = false, columnDefinition = "TEXT")
    private String postTmpContent;

    @Column(name = "tmp_post_date", nullable = false)
    private Date postTmpDate;

    @Column(name = "tmp_post_image", nullable = false)
    private String postTmpImage;

    @Column(name = "tmp_post_status", nullable = false)
    private String postTmpStatus;

    @Column(name = "tmp_post_version", nullable = false)
    private Integer postTmpVersion;

    public Integer getPostTmpID() {
        return postTmpID;
    }

    public void setPostTmpID(Integer postTmpID) {
        this.postTmpID = postTmpID;
    }

    public User getUserId() {
        return user;
    }

    public void setUserId(User userId) {
        this.user = userId;
    }

    public Category getCategoryId() {
        return category;
    }

    public void setCategoryId(Category categoryId) {
        this.category = categoryId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostTmpTitle() {
        return postTmpTitle;
    }

    public void setPostTmpTitle(String postTmpTitle) {
        this.postTmpTitle = postTmpTitle;
    }

    public String getPostTmpContent() {
        return postTmpContent;
    }

    public void setPostTmpContent(String postTmpContent) {
        this.postTmpContent = postTmpContent;
    }

    public Date getPostTmpDate() {
        return postTmpDate;
    }

    public void setPostTmpDate(Date postTmpDate) {
        this.postTmpDate = postTmpDate;
    }

    public String getPostTmpImage() {
        return postTmpImage;
    }

    public void setPostTmpImage(String postTmpImage) {
        this.postTmpImage = postTmpImage;
    }

    public String getPostTmpStatus() {
        return postTmpStatus;
    }

    public void setPostTmpStatus(String postTmpStatus) {
        this.postTmpStatus = postTmpStatus;
    }

    public Integer getPostTmpVersion() {
        return postTmpVersion;
    }

    public void setPostTmpVersion(Integer postTmpVersion) {
        this.postTmpVersion = postTmpVersion;
    }
}
