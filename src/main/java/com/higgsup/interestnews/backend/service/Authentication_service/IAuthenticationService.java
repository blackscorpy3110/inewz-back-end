package com.higgsup.interestnews.backend.service.Authentication_service;

import com.higgsup.interestnews.backend.presentation.dto.AuthenticationDTO;
import com.higgsup.interestnews.backend.presentation.dto.User.UserDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Mr.Java on 1/8/2016.
 */
public interface IAuthenticationService {

    @Transactional(readOnly = false)
//cho phep ghi
    AuthenticationDTO login(UserDTO user) throws Exception;

    UserDTO checkToken(String token) throws Exception;
}
