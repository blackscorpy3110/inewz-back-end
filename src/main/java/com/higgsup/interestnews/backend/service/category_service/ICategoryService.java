package com.higgsup.interestnews.backend.service.category_service;

import com.higgsup.interestnews.backend.presentation.dto.category.CategoryDTO;
import com.higgsup.interestnews.backend.presentation.dto.category.CategoryManageListDTO;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
public interface ICategoryService {
    public CategoryManageListDTO showAllCategory();

    public CategoryManageListDTO showOneCategory(Integer catId);

    public SimpleDTO createCategory(String field, MultipartFile file) throws Exception;

    public SimpleDTO deleteCategory(CategoryDTO categoryDTO) throws Exception;

    public SimpleDTO editCategory(String field, MultipartFile file) throws Exception;
}
