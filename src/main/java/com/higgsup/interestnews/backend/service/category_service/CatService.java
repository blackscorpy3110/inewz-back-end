package com.higgsup.interestnews.backend.service.category_service;

import com.google.gson.Gson;
import com.higgsup.interestnews.backend.presentation.dto.category.CategoryDTO;
import com.higgsup.interestnews.backend.presentation.dto.category.CategoryManageListDTO;
import com.higgsup.interestnews.backend.presentation.dto.common.SimpleDTO;
import com.higgsup.interestnews.backend.repository.CategoryRepository;
import com.higgsup.interestnews.backend.service.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

/**
 * Created by Bui Cong Thanh on 1/10/2016.
 */
@Service
@Transactional(readOnly = false)
public class CatService implements ICategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    //show all cat for manager
    @Transactional(readOnly = true)
    @Override
    public CategoryManageListDTO showAllCategory() {
        CategoryManageListDTO categoryManageListDTO = new CategoryManageListDTO();
        List<Object[]> objects = categoryRepository.showAllCategories();

        for (Object[] cat : objects) {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setCatId((Integer) cat[0]);
            categoryDTO.setCatName((String) cat[1]);
            categoryDTO.setCategoryImage((String) cat[2]);
            categoryDTO.setCatDescription((String) cat[3]);

            categoryManageListDTO.getCategoryDTOs().add(categoryDTO);
        }

        return categoryManageListDTO;
    }

    @Override
    public CategoryManageListDTO showOneCategory(Integer catId) {
        CategoryManageListDTO categoryManageListDTO = new CategoryManageListDTO();

        List<Object[]> properties = categoryRepository.getOneCategory(catId);

        for (Object[] postTmp : properties) {

            CategoryDTO postDTO = new CategoryDTO();
            postDTO.setCatId((Integer) postTmp[0]);
            postDTO.setCatName((String) postTmp[1]);
            postDTO.setCategoryImage((String) postTmp[2]);
            postDTO.setCatDescription((String) postTmp[3]);

            categoryManageListDTO.getCategoryDTOs().add(postDTO);
        }
        return categoryManageListDTO;
    }

    //create new cat
    @Override
    public SimpleDTO createCategory(String field, MultipartFile file) throws Exception {
        Gson gson = new Gson();
        CategoryDTO categoryDTO = gson.fromJson(field, CategoryDTO.class);
        SimpleDTO simpleDTO = new SimpleDTO();
        Category category = new Category();

        if (!file.isEmpty()) {
            try {
                String originalFilename = file.getOriginalFilename();
                String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
                UUID uuid = UUID.randomUUID();
                String filename = uuid.toString() + fileExtension;
                String directory = "D:/XAMPP/htdocs/interestnews/front-end/src/main/resources/static/customDesign/images/category";
                String filepath = Paths.get(directory, filename).toString();

                // Save the file locally
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                stream.write(file.getBytes());
                stream.close();

                if (categoryRepository.countByCategoryName(categoryDTO.getCatName()) > 0) {
                    throw new Exception("Chuyên mục này đã tồn tại!");
                } else {
                    category.setCategoryName(categoryDTO.getCatName());
                    category.setCategoryDes(categoryDTO.getCatDescription());
                    category.setCategoryImage(filename);
                    categoryRepository.save(category);
                    simpleDTO.setMessage("Tạo chuyên mục thành công!");
                }
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            throw new Exception("Chưa có ảnh chuyên mục !");
        }
        return simpleDTO;
    }

    //delete cat by id
    @Override
    public SimpleDTO deleteCategory(CategoryDTO categoryDTO) throws Exception {
        SimpleDTO simpleDTO = new SimpleDTO();
        categoryRepository.delete(categoryDTO.getCatId());
        if (categoryRepository.findOne(categoryDTO.getCatId()) == null) {
            simpleDTO.setMessage("Xóa chuyên mục thành công!.");
        } else {
            throw new Exception("Đã xảy ra lỗi,chưa xóa được dữ liệu.");
        }
        return simpleDTO;
    }

    //edit cat by id
    @Override
    public SimpleDTO editCategory(String field, MultipartFile file) throws Exception {

        SimpleDTO simpleDTO = new SimpleDTO();

        Gson gson = new Gson();
        CategoryDTO categoryDTO = gson.fromJson(field, CategoryDTO.class);

        try {
            String originalFilename = file.getOriginalFilename();
            String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
            UUID uuid = UUID.randomUUID();
            String filename = uuid.toString() + fileExtension;
            String directory = "D:/XAMPP/htdocs/interestnews/front-end/src/main/resources/static/customDesign/images/category";
            String filepath = Paths.get(directory, filename).toString();

            // Save the file locally
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            stream.write(file.getBytes());
            stream.close();

            int ci = categoryDTO.getCatId();
            String cn = categoryDTO.getCatName();
            String img = filename;
            String cd = categoryDTO.getCatDescription();
            if(categoryRepository.editCatByCategoryId(ci, cn, img, cd) == true ){
                simpleDTO.setMessage("Dữ liệu đã được thay đổi thành công!");
            } else {
                throw new Exception("Dữ liệu chưa được thay đổi,đã xảy ra lỗi!");
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return simpleDTO;
    }
}
