package com.higgsup.interestnews.backend.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mr.Java on 1/18/2016.
 */
public class validatorImage {
    private Matcher matcher;
    private Pattern pattern;

    private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(/bmp|jpg|gif|png))$)";

    public validatorImage() {
        pattern = pattern.compile(IMAGE_PATTERN);
    }

    public boolean validate(final String image){
        matcher = pattern.matcher(image);
        return matcher.matches();
    }
}
