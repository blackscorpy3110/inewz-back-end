package com.higgsup.interestnews.backend.common;

import org.springframework.http.HttpStatus;

public enum i18nMessage
{
	 NULL_POITER_EXCEPTION("null poiter exception", HttpStatus.NOT_FOUND),

    // Authentication Messages
    INVALID_USERNAME_OR_PASSWORD("Invalid username or password", HttpStatus.FORBIDDEN),
    NOT_LOGIN("Need to user to continue", HttpStatus.FORBIDDEN),
    TOKEN_EXPIRED_OR_DOESNT_EXIST("Token expired or doesn't exist", HttpStatus.FORBIDDEN),

    // Authorisation Messages
    NOT_AUTHORISED("Not authorised", HttpStatus.UNAUTHORIZED),

    USERNAME_NOT_FOUND("Username is not exist", HttpStatus.NOT_FOUND),
    USERNAME_ALREADY_EXISTED("Username is already", HttpStatus.CONFLICT),
    EMAIL_ALREADY_EXISTED("Email is already exist", HttpStatus.CONFLICT),

    MISSING_REQUIRED_FIELD("Missing requirement fields. All fields are required", HttpStatus.BAD_REQUEST),

    SUCCESS("Success", HttpStatus.OK);


    private String baseMessage;
    private HttpStatus httpStatus;

    i18nMessage(String baseMessage, HttpStatus httpStatus)
    {
        this.baseMessage = baseMessage;
        this.httpStatus = httpStatus;
    }

    public String getBaseMessage()
    {
        return baseMessage;
    }

    public HttpStatus getHttpStatus()
    {
        return httpStatus;
    }
}
