package com.higgsup.interestnews.backend.common;


import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Paths;

/**
 * Created by Mr.Java on 1/18/2016.
 */
public class getImage {

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private String fileName;
    private String filePath;
    private String directory = "C:/Users/Mr.Java/Desktop/uploadfile";

    public getImage() {
    }

    public getImage(MultipartFile file) {
        fileName = file.getOriginalFilename();
        filePath = Paths.get(directory,fileName).toString();
    }

    public boolean saveImage(MultipartFile file, String filepath){
        validatorImage validatorImage = new validatorImage();
        try {
            if(validatorImage.validate(file.getOriginalFilename())==true){
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                stream.write(file.getBytes());
                stream.close();
                return true;
            }else {
                return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
